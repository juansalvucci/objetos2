package ar.com.unq.ciu.test;

import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.Imperio;
import ar.com.unq.ciu.model.Tecnologia;
import ar.com.unq.ciu.model.Tesoro;
import ar.com.unq.ciu.model.edificio.EdificioCultural;
import ar.com.unq.ciu.model.edificio.EdificioEconomico;
import ar.com.unq.ciu.model.edificio.EdificioMilitar;
import ar.com.unq.ciu.model.humor.Pacifista;
import ar.com.unq.ciu.model.humor.Perseguida;
import ar.com.unq.ciu.model.humor.Sensible;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CivilizationTest {

    Tesoro tesoroImperio1 = new Tesoro(100000);
    Imperio imperio1 = new Imperio();

    Pacifista humorPacifista = new Pacifista();
    Perseguida humorPerseguida = new Perseguida();
    Sensible humorSensible = new Sensible();

    Ciudad ciudad1 = new Ciudad(humorPacifista, 15000);
    Ciudad ciudad2 = new Ciudad(humorPerseguida, 1000);
    Ciudad ciudad3 = new Ciudad(humorSensible, 1500);

    EdificioEconomico edificioCE1 = new EdificioEconomico(200, 10000, 50000);
    EdificioEconomico edificioCE2 = new EdificioEconomico(300, 10100, 50000);
    EdificioEconomico edificioCE3 = new EdificioEconomico(350, 14000, 50000);
    EdificioEconomico edificioCE4 = new EdificioEconomico(350, 14000, 10000);
    EdificioCultural edificioCC1 = new EdificioCultural(500, 20000, 20);
    EdificioCultural edificioCC2 = new EdificioCultural(600, 20100, 10);
    EdificioCultural edificioCC3 = new EdificioCultural(550, 13000, 150);
    EdificioCultural edificioCC4 = new EdificioCultural(550, 13000, 5);
    EdificioMilitar edificioCM1 = new EdificioMilitar(300, 15000, 5);
    EdificioMilitar edificioCM2 = new EdificioMilitar(700, 15100, 5);
    EdificioMilitar edificioCM3 = new EdificioMilitar(800, 16000, 5);


    @DisplayName("Tests:")
    @Test
    void testCivilization() {

        imperio1.setTesoro(tesoroImperio1);

        ciudad1.agregarEdificio(edificioCE1);
        ciudad2.agregarEdificio(edificioCE2);
        ciudad3.agregarEdificio(edificioCE3);
        ciudad3.agregarEdificio(edificioCE4);
        ciudad1.agregarEdificio(edificioCC1);
        ciudad2.agregarEdificio(edificioCC2);
        ciudad3.agregarEdificio(edificioCC3);
        ciudad3.agregarEdificio(edificioCC4);
        ciudad1.agregarEdificio(edificioCM1);
        ciudad2.agregarEdificio(edificioCM2);
        ciudad3.agregarEdificio(edificioCM3);

        ciudad1.getEdificios();
        ciudad2.getEdificios();
        ciudad3.getEdificios();

        imperio1.agregarCiudad(ciudad1);
        imperio1.agregarCiudad(ciudad2);
        imperio1.agregarCiudad(ciudad3);


        System.out.println("REQUERIMIENTO 1: ");
        System.out.println("Ciudades de imperio1: " + imperio1.getCiudadesPorCultura());
        System.out.print("Cantidad de cultura: ");
        imperio1.getCiudadesPorCultura().forEach(c -> System.out.print(c.getCantidadDeCulturaDeEdificios() + ", "));

        System.out.println(" ");
        System.out.println("===============================================================================================");

        System.out.println("REQUERIMIENTO 2: ");
        System.out.print("Edificio más valioso de ciudad 1: " + imperio1.edificioMasValioso(ciudad1));
        System.out.println(" -> Valor del edificio más valioso de ciudad 1: " + imperio1.edificioMasValioso(ciudad1).valorDeEdificio());
        System.out.print("Edificio más valioso de ciudad 2: " + imperio1.edificioMasValioso(ciudad2));
        System.out.println(" -> Valor del edificio más valioso de ciudad 2: " + imperio1.edificioMasValioso(ciudad2).valorDeEdificio());
        System.out.print("Edificio más valioso de ciudad 3: " + imperio1.edificioMasValioso(ciudad3));
        System.out.println(" -> Valor del edificio más valioso de ciudad 3: " + imperio1.edificioMasValioso(ciudad3).valorDeEdificio());

        System.out.println("===============================================================================================");

        System.out.println("REQUERIMIENTO 3: ");
        Assertions.assertTrue(ciudad1.esFeliz());
        Assertions.assertFalse(ciudad2.esFeliz());
        Assertions.assertTrue(ciudad3.esFeliz());

        System.out.println("===============================================================================================");

        System.out.println("REQUERIMIENTO 4: ");
        Tecnologia tecnologia1 = new Tecnologia("Tecnologia 1");

        //requisitos tecnologia 1:
        Tecnologia tecnologia2 = new Tecnologia("Tecnologia 2");
        Tecnologia tecnologia3 = new Tecnologia("Tecnologia 3");

        tecnologia1.agregarRequisito(tecnologia2);
        tecnologia1.agregarRequisito(tecnologia3);

        Assertions.assertFalse(imperio1.puedeIncorporar(tecnologia1));

        imperio1.agregarTecnologia(tecnologia2);
        imperio1.agregarTecnologia(tecnologia3);

        Assertions.assertTrue(imperio1.puedeIncorporar(tecnologia1));

        System.out.println("===============================================================================================");

        System.out.println("REQUERIMIENTO 5: ");
        //inciso a:
        Assertions.assertEquals((Integer) 160000, imperio1.ingresosPorTurno());
        //inciso b:
        Assertions.assertEquals((Integer) 5200, imperio1.egresosPorTurno());
        //inciso c:

        ciudad1.agregarUnidadesMilitares();
        ciudad2.agregarUnidadesMilitares();
        ciudad3.agregarUnidadesMilitares();
        Assertions.assertEquals((Integer) 15, imperio1.potenciaTotalPorTurno());

        System.out.println("===============================================================================================");

        System.out.println("REQUERIMIENTO 6: ");

        //inciso a:
        Assertions.assertEquals((Integer) 15000, ciudad1.getCantidadDeHabitantes());
        Assertions.assertEquals((Integer) 1000, ciudad2.getCantidadDeHabitantes());
        Assertions.assertEquals((Integer) 1500, ciudad3.getCantidadDeHabitantes());
        //inciso b y c:
        Assertions.assertEquals((Integer) 100000, tesoroImperio1.getCantidadDePepines());
        //inciso d:
        System.out.println("Unidades Militares PRE evolucion de turno: ");
        imperio1.getCiudades().forEach(c -> System.out.println(c.getUnidadesMilitares()));
        //inciso e:
        Assertions.assertEquals((Integer)5, edificioCC4.cantidadDeCultura());
        //inciso f:
        Assertions.assertEquals((Integer) 10000, edificioCE4.getCantidadDineroProducida());

        ciudad2.setHumor(humorPacifista);

        imperio1.evolucionPorTurno();

        //inciso a POST evolucion:
        Assertions.assertEquals((Integer) 15750, ciudad1.getCantidadDeHabitantes());
        Assertions.assertEquals((Integer) 1050, ciudad2.getCantidadDeHabitantes());
        Assertions.assertEquals((Integer) 1575, ciudad3.getCantidadDeHabitantes());
        //inciso b y c POST evolucion:
        Assertions.assertEquals((Integer) 254800, tesoroImperio1.getCantidadDePepines());
        //inciso d POST evolucion:
        System.out.println(" ");
        System.out.println("Unidades Militares POST evolucion de turno: ");
        imperio1.getCiudades().forEach(c -> System.out.println(c.getUnidadesMilitares()));
        //inciso e POST evolucion:
        Assertions.assertEquals((Integer)10, edificioCC4.cantidadDeCultura());
        //inciso f POST evolucion:
        Assertions.assertEquals((Integer) 20000, edificioCE4.getCantidadDineroProducida());
    }
}
