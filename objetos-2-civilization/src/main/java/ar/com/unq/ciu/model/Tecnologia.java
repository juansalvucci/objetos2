package ar.com.unq.ciu.model;

import java.util.ArrayList;
import java.util.Collection;

public final class Tecnologia {

    private String nombre;
    private Collection<Tecnologia> requisitosDeIncorporacion;

    public Tecnologia(String nombre) {
        super();
        this.nombre = nombre;
        this.requisitosDeIncorporacion = new ArrayList<>();
    }

    public void agregarRequisito(Tecnologia tecnologia) {
        this.getRequisitosDeIncorporacion().add(tecnologia);
    }

    public Collection<Tecnologia> getRequisitosDeIncorporacion() {
        return this.requisitosDeIncorporacion;
    }

    public void setRequisitosDeIncorporacion(Collection<Tecnologia> requisitosDeIncorporacion) {
        this.requisitosDeIncorporacion = requisitosDeIncorporacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
