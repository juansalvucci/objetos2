package ar.com.unq.ciu.model.edificio;

import ar.com.unq.ciu.model.Juego;
import ar.com.unq.ciu.model.Boost;

public final class EdificioCultural extends Edificio implements Boost {

    private Integer cantidadDeCultura;

    public EdificioCultural(Integer costoMantenimiento, Integer costoConstruccion, Integer cantidadDeCultura) {
        super(costoMantenimiento, costoConstruccion);
        this.cantidadDeCultura = cantidadDeCultura;
    }

    @Override
    public Integer cantidadDeCultura() {
        return this.cantidadDeCultura;
    }

    @Override
    public Integer nivelDeTranquilidadGenerada() {
        return this.getCantidadDeCultura() / Juego.getInstance().getFactorTranquilidad();
    }

    @Override
    public Integer valorDeEdificio() {
        return this.cantidadDeCultura;
    }

    @Override
    public void boostear() {
        this.cantidadDeCultura *= 2;
    }

    public Integer getCantidadDeCultura() {
        return cantidadDeCultura;
    }

    public void setCantidadDeCultura(Integer cantidadDeCultura) {
        this.cantidadDeCultura = cantidadDeCultura;
    }
}
