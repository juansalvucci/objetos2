package ar.com.unq.ciu.model.humor;

import ar.com.unq.ciu.model.Ciudad;

public final class Pacifista extends Humor {

    @Override
    public Integer getUnidadesDeDisconformidad(Ciudad ciudad) {
        return (ciudad.getCantidadDeHabitantes()/15000) + ciudad.getCantidadDeUnidadesMilitares();
    }
}
