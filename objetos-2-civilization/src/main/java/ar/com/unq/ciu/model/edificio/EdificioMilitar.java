package ar.com.unq.ciu.model.edificio;

import ar.com.unq.ciu.model.UnidadMilitar;

public final class EdificioMilitar extends Edificio {

    private Integer potenciaDeUnidadMilitar;

    public EdificioMilitar(Integer costoMantenimiento, Integer costoConstruccion, Integer potenciaDeUnidadMilitar) {
        super(costoMantenimiento, costoConstruccion);
        this.potenciaDeUnidadMilitar = potenciaDeUnidadMilitar;
    }

    @Override
    public Integer cantidadDeCultura() {
        return 0;
    }

    @Override
    public Integer nivelDeTranquilidadGenerada() {
        return 1;
    }

    @Override
    public Integer valorDeEdificio() {
        return this.getPotenciaDeUnidadMilitar();
    }

    public UnidadMilitar crearUnidadMilitar() {
        return new UnidadMilitar(this.potenciaDeUnidadMilitar);
    }

    public Integer getPotenciaDeUnidadMilitar() {
        return potenciaDeUnidadMilitar;
    }

    public void setPotenciaDeUnidadMilitar(Integer potenciaDeUnidadMilitar) {
        this.potenciaDeUnidadMilitar = potenciaDeUnidadMilitar;
    }
}

