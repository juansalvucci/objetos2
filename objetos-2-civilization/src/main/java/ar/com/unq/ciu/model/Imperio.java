package ar.com.unq.ciu.model;

import ar.com.unq.ciu.model.edificio.Edificio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

public final class Imperio {

    private Tesoro tesoro;
    private Collection<Ciudad> ciudades;
    private Collection<Tecnologia> tecnologias;

    public Imperio() {
        super();
        this.ciudades = new ArrayList<>();
        this.tecnologias = new ArrayList<>();
    }

    public void agregarCiudad(Ciudad ciudad) {
        this.getCiudades().add(ciudad);
    }

    public void agregarTecnologia(Tecnologia tecnologia) {
        if (this.puedeIncorporar(tecnologia)) {
            this.getTecnologias().add(tecnologia);
        } else {
            System.out.println(" Ya tiene incorporada la tecnología");
        }
    }

    // PUNTO 1:
    public Collection<Ciudad> getCiudadesPorCultura() {
        return this.getCiudades().stream()
                .sorted(Comparator.comparing(Ciudad::getCantidadDeCulturaDeEdificios).reversed())
                .collect(Collectors.toList());
    }

    // PUNTO 2:
    public Edificio edificioMasValioso(Ciudad ciudad) {
        return ciudad.getEdificios().stream()
                .max(Comparator.comparing(Edificio::valorDeEdificio)).get();
    }

    private Boolean noTieneIncorporada(Tecnologia tecnologia) {
        return !this.getTecnologias().contains(tecnologia);
    }

    private Boolean incorporoRequisitosDe(Tecnologia tecnologia) {
        return this.getTecnologias().containsAll(tecnologia.getRequisitosDeIncorporacion());
    }

    // PUNTO 4:
    public Boolean puedeIncorporar(Tecnologia nuevaTecnologia) {
        return this.noTieneIncorporada(nuevaTecnologia) && this.incorporoRequisitosDe(nuevaTecnologia);
    }

    // PUNTO 5 - a):
    public Integer ingresosPorTurno() {
        return this.ciudades.stream()
                .mapToInt(Ciudad::ingresosPorTurnoEdificiosEconomicos)
                .sum();
    }

    // PUNTO 5 - b):
    public Integer egresosPorTurno() {
        return this.ciudades.stream()
                .mapToInt(Ciudad::costoTotalMantenimientoEdificios)
                .sum();
    }

    // PUNTO 5 - c):
    public Integer potenciaTotalPorTurno() {
        return this.ciudades.stream()
                .mapToInt(Ciudad::potenciaTotalUnidadesMilitares)
                .sum();
    }

    // PUNTO 6 - b):
    private void pagarMantenimientoEdificios() {
        this.getTesoro().restarCantidadDePepines(this.egresosPorTurno());
    }

    // PUNTO 6 - c):
    private void dineroProducidoPorEdificiosEconomicos() {
        this.getTesoro().sumarCantidadDePepines(this.ingresosPorTurno());
    }

    // PUNTO 6:
    public void evolucionPorTurno() {
        this.ciudades.forEach(Ciudad::aumentarPoblacion);
        this.pagarMantenimientoEdificios();
        this.dineroProducidoPorEdificiosEconomicos();
        this.getCiudades().forEach(Ciudad::agregarUnidadesMilitares);
        this.getCiudades().forEach(Ciudad::boostearEdificioCultural);
        this.getCiudades().forEach(Ciudad::boostearEdificioEconomico);
    }

    public Collection<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(Collection<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }

    public Collection<Tecnologia> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(Collection<Tecnologia> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public Tesoro getTesoro() {
        return tesoro;
    }

    public void setTesoro(Tesoro tesoro) {
        this.tesoro = tesoro;
    }
}
