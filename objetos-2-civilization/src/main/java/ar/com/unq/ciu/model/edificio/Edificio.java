package ar.com.unq.ciu.model.edificio;

public abstract class Edificio {

    private Integer costoMantenimiento;
    private Integer costoConstruccion;

    public Edificio(Integer costoMantenimiento, Integer costoConstruccion) {
        this.costoMantenimiento = costoMantenimiento;
        this.costoConstruccion = costoConstruccion;
    }

    public abstract Integer cantidadDeCultura();

    public abstract Integer nivelDeTranquilidadGenerada();

    public abstract Integer valorDeEdificio();

    public Integer getCostoMantenimiento() {
        return costoMantenimiento;
    }

    public void setCostoMantenimiento(Integer costoMantenimiento) {
        this.costoMantenimiento = costoMantenimiento;
    }

    public Integer getCostoConstruccion() {
        return costoConstruccion;
    }

    public void setCostoConstruccion(Integer costoConstruccion) {
        this.costoConstruccion = costoConstruccion;
    }
}
