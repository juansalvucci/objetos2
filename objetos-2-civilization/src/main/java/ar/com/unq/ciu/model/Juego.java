package ar.com.unq.ciu.model;

public class Juego {

    private static Juego instance;

    private Integer factorTranquilidad;

    private Juego() {
        super();
        this.factorTranquilidad = 5;
    }

    public static Juego getInstance() {
        if (instance==null) {
            instance = new Juego();
        }
        return instance;
    }

    public Integer getFactorTranquilidad() {
        return factorTranquilidad;
    }
}

