package ar.com.unq.ciu.model.humor;

import ar.com.unq.ciu.model.Ciudad;

public final class Perseguida extends Humor {

    @Override
    public Integer getUnidadesDeDisconformidad(Ciudad ciudad) {
        if (ciudad.getCantidadDeUnidadesMilitares().equals(0)) {
            return 13 + (ciudad.getCantidadDeHabitantes()/40000);
        } else if (ciudad.getCantidadDeUnidadesMilitares()>=1 && ciudad.getCantidadDeUnidadesMilitares()<=3) {
            return 8 + (ciudad.getCantidadDeHabitantes()/40000);
        } else {
            return 3 + (ciudad.getCantidadDeHabitantes()/40000);
        }
    }
}

