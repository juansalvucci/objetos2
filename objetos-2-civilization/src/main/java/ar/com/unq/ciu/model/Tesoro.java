package ar.com.unq.ciu.model;

public final class Tesoro {

    private Integer pepines;

    public Tesoro(Integer pepines) {
        super();
        this.pepines = pepines;
    }

    public Integer getCantidadDePepines() {
        return pepines;
    }

    public void setCantidadDePepines(Integer pepines) {
        this.pepines = pepines;
    }

    public void restarCantidadDePepines(Integer pepines) {
        this.pepines -= pepines;
    }

    public void sumarCantidadDePepines(Integer pepines) {
        this.pepines += pepines;
    }
}
