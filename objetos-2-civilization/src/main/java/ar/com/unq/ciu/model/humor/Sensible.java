package ar.com.unq.ciu.model.humor;

import ar.com.unq.ciu.model.Ciudad;

public final class Sensible extends Humor {

    @Override
    public Integer getUnidadesDeDisconformidad(Ciudad ciudad) {
        if (ciudad.getCantidadDeHabitantes() <=200000) {
            return (ciudad.getCantidadDeHabitantes()/20000) - ciudad.getCantidadDeEdificiosCulturales();
        } else {
            return (10 + (200000 - ciudad.getCantidadDeHabitantes())/40000) - ciudad.getCantidadDeEdificiosCulturales();
        }
    }
}
