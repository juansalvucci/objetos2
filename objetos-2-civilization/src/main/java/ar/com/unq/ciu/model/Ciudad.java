package ar.com.unq.ciu.model;

import ar.com.unq.ciu.model.edificio.Edificio;
import ar.com.unq.ciu.model.edificio.EdificioCultural;
import ar.com.unq.ciu.model.edificio.EdificioEconomico;
import ar.com.unq.ciu.model.edificio.EdificioMilitar;
import ar.com.unq.ciu.model.humor.Humor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

public final class Ciudad {

    private Humor humor;
    private Integer cantidadDeHabitantes;
    private Collection<EdificioEconomico> edificiosEconomicos;
    private Collection<EdificioCultural> edificiosCulturales;
    private Collection<EdificioMilitar> edificiosMilitares;
    private Collection<UnidadMilitar> unidadesMilitares;

    public Ciudad(Humor humor, Integer cantidadDeHabitantes) {
        super();
        this.humor = humor;
        this.cantidadDeHabitantes = cantidadDeHabitantes;
        this.edificiosCulturales = new ArrayList<>();
        this.edificiosEconomicos = new ArrayList<>();
        this.unidadesMilitares = new ArrayList<>();
        this.edificiosMilitares = new ArrayList<>();
    }

    public Collection<Edificio> getEdificios() {
        Collection<Edificio> edificios = new ArrayList<>();
        edificios.addAll(this.getEdificiosEconomicos());
        edificios.addAll(this.getEdificiosCulturales());
        edificios.addAll(this.getEdificiosMilitares());
        return edificios;
    }

    public void agregarEdificio(EdificioCultural edificioCultural) {
        this.getEdificiosCulturales().add(edificioCultural);
    }

    public void agregarEdificio(EdificioEconomico edificioEconomico) {
        this.getEdificiosEconomicos().add(edificioEconomico);
    }

    public void agregarEdificio(EdificioMilitar edificioMilitar) {
        this.getEdificiosMilitares().add(edificioMilitar);
    }

    public Integer getCantidadDeCulturaDeEdificios() {
        return this.getEdificios().stream()
                .mapToInt(Edificio::cantidadDeCultura)
                .sum();
    }

    public Integer getNivelTranquilidadGeneradoPorEdificios() {
        return this.getEdificios().stream()
                .mapToInt(Edificio::nivelDeTranquilidadGenerada)
                .sum();
    }

    public Integer getNivelDeDisconformidad() {
        return this.humor.getUnidadesDeDisconformidad(this);
    }

    // PUNTO 3:
    public Boolean esFeliz() {
        return this.getNivelTranquilidadGeneradoPorEdificios() > this.getNivelDeDisconformidad();
    }

    // PUNTO 6 - a):
    public void aumentarPoblacion() {
        Integer cincoPorCientoPoblacion = (this.cantidadDeHabitantes * 5)/100;
        if (this.esFeliz()) {
            this.cantidadDeHabitantes += cincoPorCientoPoblacion;
        }
    }

    // PUNTO 6:
    private Boolean tieneEdificioCulturalQueIrradiaMasDe(Integer cantidad) {
        return this.edificiosCulturales.stream()
                .anyMatch(edificio -> edificio.getCantidadDeCultura() > cantidad);
    }

    private Boolean tieneEdificioEconomicoQueProduceMasDe(Integer cantidad) {
        return this.edificiosEconomicos.stream()
                .anyMatch(edificio -> edificio.getCantidadDineroProducida() > cantidad);
    }

    private EdificioCultural edificioCulturalQueMenosIrradia() {
        return this.edificiosCulturales.stream()
                .min(Comparator.comparing(EdificioCultural::cantidadDeCultura)).get();
    }

    private EdificioEconomico edificioEconomicoQueMenosDineroProduce() {
        return this.edificiosEconomicos.stream()
                .min(Comparator.comparing(EdificioEconomico::getCantidadDineroProducida)).get();
    }

    // PUNTO 6 - d):
    public void agregarUnidadesMilitares() {
        Collection<UnidadMilitar> unidades = this.getEdificiosMilitares().stream()
                .map(EdificioMilitar::crearUnidadMilitar)
                .collect(Collectors.toList());

        this.getUnidadesMilitares().addAll(unidades);
    }

    // PUNTO 6 - e):
    public void boostearEdificioCultural() {
        if (this.tieneEdificioCulturalQueIrradiaMasDe(100)) {
            this.edificioCulturalQueMenosIrradia().boostear();
        }
    }

    // PUNTO 6 - f):
    public void boostearEdificioEconomico() {
        if (this.tieneEdificioEconomicoQueProduceMasDe(200)) {
            this.edificioEconomicoQueMenosDineroProduce().boostear();
        }
    }

    public Integer getCantidadDeUnidadesMilitares() {
        return this.unidadesMilitares.size();
    }

    public Integer getCantidadDeHabitantes() {
        return cantidadDeHabitantes;
    }

    public void setCantidadDeHabitantes(Integer cantidadDeHabitantes) {
        this.cantidadDeHabitantes = cantidadDeHabitantes;
    }

    public Collection<EdificioCultural> getEdificiosCulturales() {
        return this.edificiosCulturales;
    }

    public Integer getCantidadDeEdificiosCulturales() {
        return this.getEdificiosCulturales().size();
    }

    public Collection<EdificioEconomico> getEdificiosEconomicos() {
        return edificiosEconomicos;
    }

    public Collection<EdificioMilitar> getEdificiosMilitares() {
        return edificiosMilitares;
    }

    public void setEdificiosMilitares(Collection<EdificioMilitar> edificiosMilitares) {
        this.edificiosMilitares = edificiosMilitares;
    }

    public Integer ingresosPorTurnoEdificiosEconomicos() {
        return this.getEdificiosEconomicos().stream()
                .mapToInt(EdificioEconomico::getCantidadDineroProducida)
                .sum();
    }

    public Integer costoTotalMantenimientoEdificios() {
        return this.getEdificios().stream()
                .mapToInt(Edificio::getCostoMantenimiento)
                .sum();
    }

    public Integer potenciaTotalUnidadesMilitares() {
        return this.getUnidadesMilitares().stream()
                .mapToInt(UnidadMilitar::getPotencia)
                .sum();
    }

    public Collection<UnidadMilitar> getUnidadesMilitares() {
        return unidadesMilitares;
    }

    public void setUnidadesMilitares(Collection<UnidadMilitar> unidadesMilitares) {
        this.unidadesMilitares = unidadesMilitares;
    }

    public Humor getHumor() {
        return humor;
    }

    public void setHumor(Humor humor) {
        this.humor = humor;
    }
}

