package ar.com.unq.ciu.model.edificio;

import ar.com.unq.ciu.model.Boost;

public final class EdificioEconomico extends Edificio implements Boost {

    private Integer cantidadDineroQueProduce;

    public EdificioEconomico(Integer costoMantenimiento, Integer costoConstruccion, Integer cantidadDinero) {
        super(costoMantenimiento, costoConstruccion);
        this.cantidadDineroQueProduce = cantidadDinero;
    }

    @Override
    public Integer cantidadDeCultura() {
        if (this.cantidadDineroQueProduce >= 500) {
            return 2;
        } else {
            return 3;
        }
    }

    @Override
    public Integer nivelDeTranquilidadGenerada() {
        return 0;
    }

    @Override
    public Integer valorDeEdificio() {
        return this.getCantidadDineroProducida();
    }

    public Integer getCantidadDineroProducida() {
        return cantidadDineroQueProduce;
    }

    public void setCantidadDineroProducida(Integer cantidadDineroProducida) {
        this.cantidadDineroQueProduce = cantidadDineroProducida;
    }

    @Override
    public void boostear() {
        this.cantidadDineroQueProduce *= 2;
    }
}
