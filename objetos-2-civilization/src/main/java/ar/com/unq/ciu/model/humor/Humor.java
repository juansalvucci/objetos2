package ar.com.unq.ciu.model.humor;

import ar.com.unq.ciu.model.Ciudad;

public abstract class Humor {

    public abstract Integer getUnidadesDeDisconformidad(Ciudad ciudad);
}
