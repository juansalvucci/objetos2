package ar.com.unq.ciu.test;

import ar.com.unq.ciu.model.*;
import junit.framework.TestCase;

public class Test extends TestCase {

public void test1() {
    // Imperio:
    Imperio darkSide = new Imperio();

    // Gobernante:
    Gobernante lordVadder = new Gobernante("Anakin", 123456, "Emperador");

    // Milicias:
    Milicia milicia1 = new Milicia("Pedro Rodriguez", 645798);
    Milicia milicia2 = new Milicia("Carlos Gonzalez", 645123);
    Milicia milicia3 = new Milicia("Juan Garcia", 645321);
    Milicia milicia4 = new Milicia("Antonio Fernandez", 645654);

    // Aldeano:
    Aldeano aldeano1 = new Aldeano("Gustavo Rodriguez", 798645, true);
    Aldeano aldeano2 = new Aldeano("Sebastian Gonzalez", 123645, true);
    Aldeano aldeano3 = new Aldeano("Martin Garcia", 321645, false);
    Aldeano aldeano4 = new Aldeano("Alberto Fernandez", 654645, false);

    darkSide.agregar(milicia1);
    darkSide.agregar(milicia2);
    darkSide.agregar(milicia3);
    darkSide.agregar(milicia4);
    darkSide.agregar(aldeano1);
    darkSide.agregar(aldeano2);
    darkSide.agregar(aldeano3);
    darkSide.agregar(aldeano4);


    assertEquals(Integer.valueOf(8), darkSide.cantidadDeCiudadanos());
    assertEquals(2.0, darkSide.poderDeAtaque());
    assertEquals(2.6, darkSide.poderDeDefensa());
    }
}
