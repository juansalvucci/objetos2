package ar.com.unq.ciu.model;

/**
 *  Singleton SiluetaGuerrera
 */

public class SiluetaGuerrera {

    private Double resistencia;

    private static class SingletonHolder {
        public static final SiluetaGuerrera instance = new SiluetaGuerrera();
    }

    public static SiluetaGuerrera getInstance() {
        return SingletonHolder.instance;
    }

    private SiluetaGuerrera() {
        this.resistencia = 100.00;
    }

    public Double danioProducidoPor(Militar militar) {
        return this.resistencia * militar.poderDeGenerarDanio();
    }

    public Double getResistencia() {
        return resistencia;
    }

    public void setResistencia(Double resistencia) {
        this.resistencia = resistencia;
    }
}
