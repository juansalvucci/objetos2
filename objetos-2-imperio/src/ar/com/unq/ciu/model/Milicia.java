package ar.com.unq.ciu.model;

import java.util.ArrayList;
import java.util.Collection;

public final class Milicia extends Ciudadano implements Militar {

    private Collection<String> armas;

    public Milicia(String nombre, Integer legajo) {
        super(nombre, legajo);
        this.armas = new ArrayList<>();
    }

    @Override
    public Double poderDeGenerarDanio() {
        return 0.5 + this.armas.size() * 0.1;
    }
}
