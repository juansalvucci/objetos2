package ar.com.unq.ciu.model;

import java.util.ArrayList;
import java.util.Collection;

public final class Imperio {

    private Collection<Gobernante> gobernantes;
    private Collection<Aldeano> aldeanos;
    private Collection<Milicia> milicias;

    public Imperio() {
        this.gobernantes = new ArrayList<>();
        this.aldeanos = new ArrayList<>();
        this.milicias = new ArrayList<>();
    }

    public void agregar(Gobernante gobernante) {
        if (!this.existeLegajo(gobernante.getLegajo())) {
            this.gobernantes.add(gobernante);
        } else {
            System.out.println("Error: Legajo existente.");
        }
    }

    public void agregar(Aldeano aldeano) {
        if (!this.existeLegajo(aldeano.getLegajo())) {
            this.aldeanos.add(aldeano);
        } else {
            System.out.println("Error: Legajo existente.");
        }
    }

    public void agregar(Milicia militar) {
        if (!this.existeLegajo(militar.getLegajo())) {
            this.milicias.add(militar);
        } else {
            System.out.println("Error: Legajo existente.");
        }
    }

    private boolean existeLegajo(Integer legajo) {
        this.getCiudadanos().stream()
                .map(Ciudadano::getLegajo)
                .anyMatch(l -> l.equals(legajo));
        return false;
    }

    public Collection<Ciudadano> getCiudadanos() {
        Collection<Ciudadano> ciudadanos = new ArrayList<>();
        ciudadanos.addAll(this.gobernantes);
        ciudadanos.addAll(this.aldeanos);
        ciudadanos.addAll(this.milicias);
        return ciudadanos;
    }

    public Integer cantidadDeCiudadanos() {
        return this.getCiudadanos().size();
    }

    public Double poderDeAtaque() {
        return this.milicias.stream()
                .mapToDouble(Milicia::poderDeGenerarDanio)
                .sum();
    }

    public Double poderDeDefensa() {
        Collection<Militar> ejercito = new ArrayList<>();
        ejercito.addAll(this.milicias);
        ejercito.addAll(this.aldeanos);
        return ejercito.stream()
                .mapToDouble(Militar::poderDeGenerarDanio)
                .sum();
    }
}
