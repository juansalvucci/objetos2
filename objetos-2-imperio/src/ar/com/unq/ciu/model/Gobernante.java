package ar.com.unq.ciu.model;

public final class Gobernante extends Ciudadano {

    private String cargo;

    public Gobernante(String nombre, Integer legajo, String cargo) {
        super(nombre, legajo);
        this.cargo = cargo;
    }
}
