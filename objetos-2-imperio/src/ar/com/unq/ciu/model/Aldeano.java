package ar.com.unq.ciu.model;

public final class Aldeano extends Ciudadano implements Militar {

    private Boolean estaCansado;

    public Aldeano(String nombre, Integer legajo, Boolean estaCansado) {
        super(nombre, legajo);
        this.estaCansado = estaCansado;
    }

    @Override
    public Double poderDeGenerarDanio() {
        if (this.estaCansado) {
            return 0.1;
        } else {
            return 0.2;
        }
    }
}
