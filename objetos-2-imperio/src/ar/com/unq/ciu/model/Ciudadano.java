package ar.com.unq.ciu.model;

public abstract class Ciudadano {
    protected String nombre;
    protected Integer legajo;

    public Ciudadano(String nombre, Integer legajo) {
        this.nombre = nombre;
        this.legajo = legajo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getLegajo() {
        return legajo;
    }

    public void setLegajo(Integer legajo) {
        this.legajo = legajo;
    }
}
