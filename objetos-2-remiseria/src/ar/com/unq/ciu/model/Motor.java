package ar.com.unq.ciu.model;

public final class Motor {

    private Integer velocidadMaxima;
    private Integer peso;

    public Motor(Integer velocidadMaxima, Integer peso) {
        this.velocidadMaxima = velocidadMaxima;
        this.peso = peso;
    }

    public Integer getVelocidadMaxima() {
        return velocidadMaxima;
    }

    public void setVelocidadMaxima(Integer velMaxima) {
        this.velocidadMaxima = velMaxima;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }
}
