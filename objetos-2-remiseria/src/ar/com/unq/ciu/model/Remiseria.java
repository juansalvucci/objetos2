package ar.com.unq.ciu.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

public final class Remiseria {

    private Collection<Vehiculo> flota;
    private Collection<Viaje> viajesHechos;
    private Integer valorPorKilometro;
    private Integer precioMinimo;

    public Remiseria(Integer valorPorKilometro, Integer precioMinimo) {
        this.flota = new ArrayList<Vehiculo>();
        this.viajesHechos = new ArrayList<Viaje>();
        this.valorPorKilometro = valorPorKilometro;
        this.precioMinimo = precioMinimo;
    }

    public void agregarAFlota(Vehiculo vehiculo) {
        this.flota.add(vehiculo);
    }

    public void quitarDeFlota(Vehiculo vehiculo) {
        this.flota.remove(vehiculo);
    }

    public Integer pesoTotalFlota() {
        return this.flota.stream()
                .mapToInt(Vehiculo::getPeso)
                .sum();
    }

    public Boolean esRecomendable() {
        return this.flota.size() >= 3 &&
                this.flota.stream()
                        .allMatch(vehiculo -> vehiculo.getVelocidadMaxima() >= 100);
    }

    public Integer capacidadTotalYendoA(Integer velocidad) {
        return this.flota.stream()
                .filter(vehiculo -> vehiculo.getVelocidadMaxima() >= velocidad)
                .mapToInt(Vehiculo::getCapacidad)
                .sum();
    }

    public String colorDelAutoMasRapido() {
        Vehiculo autoMasVeloz = this.flota.stream()
                .max(Comparator.comparing(Vehiculo::getVelocidadMaxima))
                .get();

        return autoMasVeloz.color;
    }

    public Collection<Vehiculo> autosQuePuedenViajar(Viaje viaje) {
        return this.flota.stream()
                .filter(vehiculo -> viaje.puedeViajar(vehiculo))
                .collect(Collectors.toList());
    }

    // PUNTO D:
    public void registrarViaje(Viaje viaje, Vehiculo vehiculo) {
        if (viaje.puedeViajar(vehiculo)) {
            this.viajesHechos.add(viaje);
            viaje.setVehiculoAsignado(vehiculo);
        }
    }

    private Collection<Viaje> viajesQueHizo(Vehiculo vehiculo) {
        return this.viajesHechos.stream()
                .filter(viaje -> viaje.getVehiculoAsignado().equals(vehiculo))
                .collect(Collectors.toList());
    }

    public Integer cantidadDeViajes(Vehiculo vehiculo) {
        return this.viajesQueHizo(vehiculo).size();
    }

    public Integer cantidadDeViajesDeMasDe(Integer kilometros) {
        Collection<Viaje> viajes = this.viajesHechos.stream()
                .filter(viaje -> viaje.getKilometros() > kilometros)
                .collect(Collectors.toList());

        return viajes.size();
    }

    public Integer cantidadDeLugaresLibres() {
        return this.viajesHechos.stream()
                .mapToInt(Viaje::cantidadDeLugaresLibres)
                .sum();
    }

    public Integer cuantoPagarA(Vehiculo vehiculo) {
        Integer valorTotalDeViajes = this.viajesQueHizo(vehiculo).stream()
                .mapToInt(viaje -> viaje.getKilometros() * valorPorKilometro)
                .sum();

        if (valorTotalDeViajes > precioMinimo) {
            return valorTotalDeViajes;
        } else {
            return precioMinimo;
        }
    }

    public Collection<Vehiculo> getFlota() {
        return flota;
    }

    public void setFlota(Collection<Vehiculo> flota) {
        this.flota = flota;
    }

    public Collection<Viaje> getViajesHechos() {
        return viajesHechos;
    }

    public void setViajesHechos(Collection<Viaje> viajesHechos) {
        this.viajesHechos = viajesHechos;
    }

    public Integer getValorPorKilometro() {
        return valorPorKilometro;
    }

    public void setValorPorKilometro(Integer valorPorKilometro) {
        this.valorPorKilometro = valorPorKilometro;
    }

    public Integer getPrecioMinimo() {
        return precioMinimo;
    }

    public void setPrecioMinimo(Integer precioMinimo) {
        this.precioMinimo = precioMinimo;
    }
}
