package ar.com.unq.ciu.model;

public final class Interior {

    private Integer capacidad;
    private Integer peso;

    public Interior(Integer capacidad, Integer peso) {
        this.capacidad = capacidad;
        this.peso = peso;
    }

    public Integer getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Integer capacidad) {
        this.capacidad = capacidad;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }
}
