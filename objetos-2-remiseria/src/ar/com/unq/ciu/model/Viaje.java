package ar.com.unq.ciu.model;

import java.util.ArrayList;
import java.util.Collection;

public final class Viaje {

    private Integer kilometros;
    private Double tiempoMaximo; // (Horas)
    private Integer cantidadPasajeros;
    private Collection<String> coloresIncompatibles;
    private Vehiculo vehiculoAsignado;

    public Viaje(Integer kilometros, Double tiempoMaximo, Integer cantidadPasajeros) {
        this.kilometros = kilometros;
        this.tiempoMaximo = tiempoMaximo;
        this.cantidadPasajeros = cantidadPasajeros;
        this.coloresIncompatibles = new ArrayList<String>();
    }

    public Boolean puedeViajar(Vehiculo vehiculo) {
        // Condiciones:
        Boolean velocidad = vehiculo.getVelocidadMaxima() >= this.velocidadPromedio() + 10;
        Boolean capacidad = vehiculo.getCapacidad() >= this.cantidadPasajeros;
        Boolean color = !coloresIncompatibles.contains(vehiculo.color);

        return velocidad && capacidad && color;
    }

    public Double velocidadPromedio() {
        return kilometros / tiempoMaximo;
    }

    public Integer cantidadDeLugaresLibres() {
        return vehiculoAsignado.getCapacidad() - cantidadPasajeros;
    }

    public void asignarColorIncompatible(String color) {
        this.coloresIncompatibles.add(color);
    }

    public Integer getKilometros() {
        return kilometros;
    }

    public void setKilometros(Integer kilometros) {
        this.kilometros = kilometros;
    }

    public Double getTiempoMaximo() {
        return tiempoMaximo;
    }

    public void setTiempoMaximo(Double tiempoMaximo) {
        this.tiempoMaximo = tiempoMaximo;
    }

    public Integer getCantidadPasajeros() {
        return cantidadPasajeros;
    }

    public void setCantidadPasajeros(Integer cantidadPasajeros) {
        this.cantidadPasajeros = cantidadPasajeros;
    }

    public Collection<String> getColoresIncompatibles() {
        return coloresIncompatibles;
    }

    public void setColoresIncompatibles(Collection<String> coloresIncompatibles) {
        this.coloresIncompatibles = coloresIncompatibles;
    }

    public Vehiculo getVehiculoAsignado() {
        return vehiculoAsignado;
    }

    public void setVehiculoAsignado(Vehiculo vehiculoAsignado) {
        this.vehiculoAsignado = vehiculoAsignado;
    }
}
