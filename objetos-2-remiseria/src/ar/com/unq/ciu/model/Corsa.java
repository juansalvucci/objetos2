package ar.com.unq.ciu.model;

public final class Corsa extends Vehiculo {

    public Corsa(String color) {
        this.color = color;
    }

    @Override
    public Integer getCapacidad() {
        return 4;
    }

    @Override
    public Integer getVelocidadMaxima() {
        return 150;
    }

    @Override
    public Integer getPeso() {
        return 1300;
    }
}
