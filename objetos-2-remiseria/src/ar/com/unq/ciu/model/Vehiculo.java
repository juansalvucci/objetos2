package ar.com.unq.ciu.model;

public abstract class Vehiculo {

    protected Integer capacidad;
    protected Integer velocidadMaxima; // Km/h
    protected String color;
    protected Integer peso; // Kg

    public Vehiculo() {
    }

    public abstract Integer getCapacidad();

    public abstract Integer getVelocidadMaxima();

    public abstract Integer getPeso();

    public void setCapacidad(Integer capacidad) {
        this.capacidad = capacidad;
    }

    public void setVelocidadMaxima(Integer velMaxima) {
        this.velocidadMaxima = velMaxima;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }
}
