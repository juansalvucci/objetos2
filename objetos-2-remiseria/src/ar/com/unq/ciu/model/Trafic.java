package ar.com.unq.ciu.model;

/**
 *  Singleton Trafic
 */

public class Trafic extends Vehiculo {

    private Interior interior;
    private Motor motor;

    private static class SingletonHolder {
        public static final Trafic instance = new Trafic();
    }

    public static Trafic getInstance() {
        return SingletonHolder.instance;
    }

    private Trafic() {
        this.color = "blanco";
    }

    @Override
    public Integer getCapacidad() {
        return this.interior.getCapacidad();
    }

    @Override
    public Integer getVelocidadMaxima() {
        return this.motor.getVelocidadMaxima();
    }

    @Override
    public Integer getPeso() {
        return 4000 + this.interior.getPeso() + this.motor.getPeso();
    }

    public Interior getInterior() {
        return interior;
    }

    public void setInterior(Interior interior) {
        this.interior = interior;
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }
}
