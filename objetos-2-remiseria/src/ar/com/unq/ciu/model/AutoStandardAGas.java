package ar.com.unq.ciu.model;

public final class AutoStandardAGas extends Vehiculo {

    private Boolean tieneTanqueAdicional;

    public AutoStandardAGas(Boolean tieneTanqueAdicional) {
        this.color = "azul";
        this.tieneTanqueAdicional = tieneTanqueAdicional;
    }

    @Override
    public Integer getCapacidad() {
        if(this.tieneTanqueAdicional.equals(Boolean.TRUE)) {
            this.capacidad = 3;
        } else {
            this.capacidad = 4;
        }
        return this.capacidad;
    }

    @Override
    public Integer getVelocidadMaxima() {
        if (this.tieneTanqueAdicional.equals(Boolean.TRUE)) {
            this.velocidadMaxima = 120;
        } else {
            this.velocidadMaxima = 110;
        }
        return this.velocidadMaxima;
    }

    @Override
    public Integer getPeso() {
        if (this.tieneTanqueAdicional.equals(Boolean.TRUE)) {
            this.peso = 1350;
        } else {
            this.peso = 1200;
        }
        return this.peso;
    }

    public Boolean getTieneTanqueAdicional() {
        return tieneTanqueAdicional;
    }

    public void setTieneTanqueAdicional(Boolean tieneTanqueAdicional) {
        this.tieneTanqueAdicional = tieneTanqueAdicional;
    }
}
