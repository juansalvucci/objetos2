package ar.com.unq.ciu.model;

public final class AutoDistinto extends Vehiculo {

    public AutoDistinto(Integer capacidad, Integer velocidadMaxima, Integer peso, String color) {
        this.capacidad = capacidad;
        this.velocidadMaxima = velocidadMaxima;
        this.peso = peso;
        this.color = color;
    }

    @Override
    public Integer getCapacidad() {
        return capacidad;
    }

    @Override
    public Integer getVelocidadMaxima() {
        return velocidadMaxima;
    }

    @Override
    public Integer getPeso() {
        return peso;
    }
}
