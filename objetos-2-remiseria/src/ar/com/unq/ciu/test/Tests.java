package ar.com.unq.ciu.test;

import ar.com.unq.ciu.model.*;
import junit.framework.TestCase;

import java.util.Arrays;

public class Tests extends TestCase {

    public void test1() {
        // REMISERIAS:
        Remiseria laLuna = new Remiseria(3,30);
        Remiseria elCielo = new Remiseria(5,50);

        // INTERIORES DE TRAFIC:
        Interior comodo = new Interior(5,700);
        Interior popular = new Interior(12,1000);

        // MOTORES DE TRAFIC:
        Motor pulenta = new Motor(130,800);
        Motor bataton = new Motor(80,500);

        // VEHICULOS:
        Vehiculo cachito = new Corsa("rojo");
        Vehiculo cacheto = new Corsa("negro");
        Vehiculo cachuto = new Corsa("verde");
        Vehiculo albondiga = new AutoStandardAGas(true);
        Vehiculo cachivache = new AutoDistinto(5, 160, 1200, "beige");
        Vehiculo carromato = new AutoStandardAGas(true);
        Vehiculo catramina = new AutoStandardAGas(false);
        Vehiculo sardina = new AutoStandardAGas(false);
        Trafic mofletes = Trafic.getInstance();
        mofletes.setInterior(comodo);
        mofletes.setMotor(bataton);


        laLuna.agregarAFlota(cachito);
        laLuna.agregarAFlota(cacheto);
        laLuna.agregarAFlota(cachuto);
        laLuna.agregarAFlota(albondiga);
        laLuna.agregarAFlota(cachivache);

        elCielo.agregarAFlota(cachito);
        elCielo.agregarAFlota(carromato);
        elCielo.agregarAFlota(catramina);
        elCielo.agregarAFlota(sardina);
        elCielo.agregarAFlota(Trafic.getInstance());


        // PUNTO B:
        assertEquals((Integer)6450, laLuna.pesoTotalFlota());
        assertEquals((Integer)10250, elCielo.pesoTotalFlota());
        assertTrue(laLuna.esRecomendable());
        assertFalse(elCielo.esRecomendable());
        assertEquals((Integer)17, laLuna.capacidadTotalYendoA(140));
        assertEquals((Integer)4, elCielo.capacidadTotalYendoA(140));
        assertEquals("beige", laLuna.colorDelAutoMasRapido());
        assertEquals("rojo", elCielo.colorDelAutoMasRapido());


        // PUNTO C:

        // VIAJES:
        Viaje viaje1 = new Viaje(112, 2.0, 3);
        Viaje viaje2 = new Viaje(100, 1.5, 4);
        Viaje viaje3 = new Viaje(50, 0.5, 4);
        Viaje viaje4 = new Viaje(200, 2.5, 3);
        Viaje viaje5 = new Viaje(350, 4.0, 4);
        Viaje viaje6 = new Viaje(7, 0.25, 3);

        viaje1.asignarColorIncompatible("negro");
        viaje2.asignarColorIncompatible("rojo");
        viaje2.asignarColorIncompatible("negro");
        viaje3.asignarColorIncompatible("beige");
        viaje4.asignarColorIncompatible("azul");
        viaje5.asignarColorIncompatible("verde");
        viaje5.asignarColorIncompatible("verde");


        assertTrue(viaje1.puedeViajar(cachito));
        assertFalse(viaje2.puedeViajar(cacheto));
        assertTrue(viaje3.puedeViajar(cachuto));
        assertTrue(viaje4.puedeViajar(cachivache));
        assertFalse(viaje5.puedeViajar(mofletes));


        assertEquals(Arrays.asList(cachito, cachuto, albondiga, cachivache), laLuna.autosQuePuedenViajar(viaje1));
        assertEquals(Arrays.asList(cachuto, cachivache), laLuna.autosQuePuedenViajar(viaje2));
        assertEquals(Arrays.asList(cachito, carromato, catramina, sardina, mofletes ), elCielo.autosQuePuedenViajar(viaje1));
        assertEquals(Arrays.asList(cachito, catramina, sardina), elCielo.autosQuePuedenViajar(viaje3));
        assertEquals(Arrays.asList(cachito), elCielo.autosQuePuedenViajar(viaje4));
        assertEquals(Arrays.asList(cachito, catramina, sardina), elCielo.autosQuePuedenViajar(viaje5));


        // PUNTO D:

        laLuna.registrarViaje(viaje1, cachito);
        laLuna.registrarViaje(viaje2, cachivache);
        laLuna.registrarViaje(viaje3, cachuto);
        laLuna.registrarViaje(viaje6, albondiga);
        elCielo.registrarViaje(viaje4, cachito);
        elCielo.registrarViaje(viaje5, cachito);


        assertEquals((Integer)1, laLuna.cantidadDeViajes(cachito));
        assertEquals((Integer)2, elCielo.cantidadDeViajes(cachito));

        assertEquals(Integer.valueOf(2), laLuna.cantidadDeViajesDeMasDe(80));
        assertEquals(Integer.valueOf(3), laLuna.cantidadDeViajesDeMasDe(40));
        assertEquals(Integer.valueOf(1), elCielo.cantidadDeViajesDeMasDe(250));
        assertEquals(Integer.valueOf(2), elCielo.cantidadDeViajesDeMasDe(100));

        assertEquals(Integer.valueOf(2), laLuna.cantidadDeLugaresLibres());
        assertEquals(Integer.valueOf(1), elCielo.cantidadDeLugaresLibres());

        assertEquals(Integer.valueOf(336), laLuna.cuantoPagarA(cachito));
        assertEquals(Integer.valueOf(300), laLuna.cuantoPagarA(cachivache));
        assertEquals(Integer.valueOf(150), laLuna.cuantoPagarA(cachuto));
        assertEquals(Integer.valueOf(30), laLuna.cuantoPagarA(albondiga));
        assertEquals(Integer.valueOf(2750), elCielo.cuantoPagarA(cachito));
    }
}