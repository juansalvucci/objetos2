class EstacionDeServicio {
    constructor(listaDispositivos){
        this._dispositivos = listaDispositivos
        this._animalesAtendidos = []
    }

    puedeSerAtendido(unAnimal) {
		return this._dispositivos.some(d => d.puedeAtenderA(unAnimal))
	}
	
	atenderA(unAnimal) {
		let algunDispositivo = this._dispositivos.find(d => d.puedeAtenderA(unAnimal))

		if(this.puedeSerAtendido(unAnimal)) {
			algunDispositivo.atender(unAnimal)
			this.agregarAnimalAtendido(unAnimal) 
		} else {
			console.log("No puede ser atendido")
		}	
	}
	
	agregarDispositivo(unDispositivo) {
		this._dispositivos.push(unDispositivo)
	}	

	agregarAnimalAtendido(unAnimal) {		
		this._animalesAtendidos.push(unAnimal) 				
	}
	
	recargarDispositivos() {
		this._dispositivos.forEach(d => d.recargar())
	}	

    fueAtendido(unAnimal) {
        return this._animalesAtendidos.includes(unAnimal)
    }
	
	animalesQueConvieneVacunar() {
		return this._animalesAtendidos.filter(animal => animal.convieneVacunar())
	}
	
	animalMasPesadoQueAtendio() {
		return this._animalesAtendidos.reduce((max, a) => (a._peso > max._peso) ? a : max)
	}
	
	pesoTotalDeAtendidos() {
		return this._animalesAtendidos.reduce((total, a) => total += a._peso, 0)
    }
}

// ESTACIONES:
var estacionDeServicio1 = new EstacionDeServicio([comederoNormal, comederoNormal2, bebedero])  // ESTACION 1 	
var estacionDeServicio2 = new EstacionDeServicio([comederoInteligente, vacunatorio])  // ESTACION 2
