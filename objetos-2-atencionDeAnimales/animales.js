class Animal {
    constructor(peso) {
        this._peso = peso
        this._tieneHambre = false
        this._tieneSed = false
        this._estaVacunado = false
        this._estaFeliz = false
    }    

    estaFeliz() {
        return this.convieneVacunar = false && (this._peso < 10 || this._peso > 150)
    }
}


class Vaca extends Animal {
    constructor(peso) {
        super(peso)        
    }

    comer(pesoAlimento) {
        this._peso += pesoAlimento / 3
        this._tieneSed = true
    }

    beber() {
        this._peso -= 0.5
        this._tieneSed = false
    }

    convieneVacunar() {
        return !this._estaVacunado
    }

    vacunar() {
		this._estaVacunado = true
	}

    tieneHambre() {
        return this._peso < 200
    }

    caminar() {
        this._peso -= 3
    }
}


class VacaZen extends Vaca {
    constructor(peso) {
        super(peso)        
    }

	vacunar() {
		super.vacunar()
		this._tieneSed = false
	}
	
	comer(pesoAlimento) {
		super.comer(pesoAlimento)
		if (this._estaVacunado) {
			this._tieneSed = false
		}		
	}
}


class VacaReflexiva extends Vaca {
    constructor(peso) {
        super(peso)
        this._cantVecesQueCamino = 0        
    }

	estaFeliz() {
        super.estaFeliz() && this._cantVecesQueCamino >= 2
    }

    caminar() {
        super.caminar()
        this._cantVecesQueCamino += 1
    }
}


class VacaFilosofa extends VacaReflexiva {
    constructor(peso) {
        super(peso)        
    }

	estaFeliz() {
        super.estaFeliz() && this._tieneSed === false
    }
}


class Cerdo extends Animal {
    constructor(peso) {
        super(peso)
        this._cantVecesComeSinBeber = 0
        this._cantMaxComida = 0        
    }

    comer(pesoAlimento) {
        if (pesoAlimento > 0.2) {
			this._peso += pesoAlimento - 0.2			
		}
		if (pesoAlimento > 1) {
			this._tieneHambre = true
		} else {
			this._tieneHambre = false
		}
		this._cantVecesComeSinBeber += 1
		if(this._cantVecesComeSinBeber > 3) {
			this._tieneSed = true
		}
		if (pesoAlimento > this._cantMaxComida ) {
			this._cantMaxComida = pesoAlimento
		}
    }

    tieneHambre() {
        return this._tieneHambre
    }

    beber() {
        this._tieneSed = false
		this._tieneHambre = true
		this._peso -= 1
		this._cantVecesComeSinBeber = 0
    }

    convieneVacunar() {
        return true
    }    
}


class CerdoResistente extends Cerdo {
    constructor(peso) {
        super(peso)               
    }
	
	comer(unosKilos) {		
		super.comer(unosKilos)
		if (unosKilos >= 5) {
			this._estaVacunado = false
		}
	}
	
	convieneVacunar() {
		return !this._estaVacunado
	}
}


class CerditoAlegre extends Cerdo {
    constructor(peso) {
        super(peso)               
    }

    estaFeliz() {
        return this._estaFeliz = true
    }
}


class Gallina extends Animal {
    constructor() {
        super()
        this._peso = 4		
	    this._cantVecesQueComio = 0	
        this._tieneHambre = true
        this._tieneSed = false
    }

	comer(pesoAlimento) {
		this._cantVecesQueComio += 1
	}	
	
	beber() {		
	}    
	
	convieneVacunar() {
		return false
	}	    
}


class GallinaTuruleca extends Gallina {	
    constructor() {
        super()
        this._cantidadDeHuevosTotal = 0
    }	
	
	haPuestoUnHuevo() {		
	    this._cantidadDeHuevosTotal += 1
	}
	
	haPuestoDosHuevos() {
		this._cantidadDeHuevosTotal += 2
	}
	
	haPuestoTresHuevos() {
		this._cantidadDeHuevosTotal += 3
	}
	
	tieneSed() {
		if(this._cantidadDeHuevosTotal % 2 == 0) {
			this._tieneSed = false
		} else {
			this._tieneSed = true
		}
	}
}



// ANIMALES:
var lola = new Vaca(120)
var pochoclo = new Vaca(210)
var porky = new Cerdo(120)
var babe = new Cerdo(60)
var pollito = new Gallina()

lola._tieneSed = true
lola._estaVacunado = true
lola._tieneHambre = true
pochoclo._tieneSed = true
pochoclo._estaVacunado = true
pochoclo._tieneHambre = false
porky._tieneSed = true
porky._tieneHambre = true
babe._tieneSed = false
babe._tieneHambre = false
