class Dispositivo {
    constructor() {
    }    
}

class ComederoNormal extends Dispositivo {
    constructor(pesoDeLaRacion, maximoPesoSoportado, cantidadDeRaciones) {
        super()
        this._pesoDeLaRacion = pesoDeLaRacion
        this._maximoPesoSoportado = maximoPesoSoportado
        this._cantidadDeRaciones = cantidadDeRaciones
    }

    necesitaRecarga() {
		return this._cantidadDeRaciones < 10
	}

    recargar() {
        if(this._necesitaRecarga) {
            this._cantidadDeRaciones += 30            
        }
    }

    atender(unAnimal) {
		if (this.puedeAtenderA(unAnimal)) {
			unAnimal.comer(this._pesoDeLaRacion)
			this._cantidadDeRaciones -= 1
		}
	}

    puedeAtenderA(unAnimal) {
		return unAnimal._tieneHambre && unAnimal._peso < this._maximoPesoSoportado
	}	    
	
}

class ComederoInteligente extends Dispositivo {
    constructor(kilosDeComida, capacidadMaxima) {
        super()
        this._kilosDeComida = kilosDeComida
        this._capacidadMaxima = capacidadMaxima      
    }

    necesitaRecarga() {
		return this._kilosDeComida < 15
	}

    recargar() {
        if(this._necesitaRecarga) {
            this._kilosDeComida = this._capacidadMaxima           
        }
    }

    atender(unAnimal) {
        let pesoRacion = unAnimal._peso / 100
		if (this.puedeAtenderA(unAnimal)) {
			unAnimal.comer(pesoRacion)	
            this._kilosDeComida -= pesoRacion
		}
	}

    puedeAtenderA(unAnimal) {
		return unAnimal._tieneHambre
	}	    
}

class Bebedero extends Dispositivo {
    constructor() {
        super()
        this._cantAnimalesAtendidos = 0             
    }

    necesitaRecarga() {
		return this.cantAnimalesAtendidos === 20
	}

    recargar() {
        if(this._necesitaRecarga) {
            this.cantAnimalesAtendidos = 0          
        }
    }

    atender(unAnimal) {        
		if (this.puedeAtenderA(unAnimal)) {
			unAnimal.beber()
            this._cantAnimalesAtendidos += 1
		}
	}

    puedeAtenderA(unAnimal) {
		return unAnimal._tieneSed
	}
}

class Vacunatorio extends Dispositivo {
    constructor(cantidadVacunas) {
        super()
        this._cantidadVacunas = cantidadVacunas            
    }

    necesitaRecarga() {
		return this._cantidadVacunas === 0
	}

    recargar() {
        if(this.necesitaRecarga()) {
            this._cantidadVacunas = 50          
        }
    }

    atender(unAnimal) {        
		if (this.puedeAtenderA(unAnimal)) {
			unAnimal._estaVacunado = true
            this._cantidadVacunas -= 1
		}
	}

    puedeAtenderA(unAnimal) {
		return unAnimal.convieneVacunar()
	}	
    
}


// DISPOSITIVOS:
var comederoNormal = new ComederoNormal(800, 70, 8)
var comederoNormal2 = new ComederoNormal(600, 110, 34)
var comederoInteligente = new ComederoInteligente(200, 400)
var bebedero = new Bebedero()
var vacunatorio = new Vacunatorio(vacunas = 14)
