package ar.com.unq.ciu.model;

import ar.com.unq.ciu.model.edificio.Edificio;
import ar.com.unq.ciu.model.humor.Humor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

public class Imperio {

    private Tesoro tesoro;
    private static Integer factorTranquilidad;
    private Humor humor;
    private Collection<Ciudad> ciudades;
    private Collection<Tecnologia> tecnologias;

    public Imperio(Tesoro tesoro, Integer factorTranquilidad, Humor humor) {
        super();
        this.tesoro = tesoro;
        this.factorTranquilidad = factorTranquilidad;
        this.humor = humor;
        this.ciudades = new ArrayList<>();
        this.tecnologias = new ArrayList<>();
    }

    public void agregarCiudad(Ciudad ciudad) {
        this.getCiudades().add(ciudad);
    }

    public void agregarTecnologia(Tecnologia tecnologia) {
        if (this.puedeIncorporar(tecnologia)) {
            this.getTecnologias().add(tecnologia);
        } else {
            System.out.println(" No es posible incorporar la tecnología");
        }
    }

    public Collection<Ciudad> getCiudadesPorCultura() {
        return this.getCiudades().stream()
                .sorted(Comparator.comparing(Ciudad::getCantidadDeCulturaDeEdificios).reversed())
                .collect(Collectors.toList());
    }

    public Edificio edificioMasValioso(Ciudad ciudad) {
        return ciudad.getEdificios().stream()
                .max(Comparator.comparing(Edificio::valorDeEdificio)).get();
    }

    private Boolean noTieneIncorporada(Tecnologia tecnologia) {
        return !this.getTecnologias().contains(tecnologia);
    }

    private Boolean incorporoRequisitosDe(Tecnologia tecnologia) {
        return this.getTecnologias().containsAll(tecnologia.getRequisitosDeIncorporacion());
    }

    public Boolean puedeIncorporar(Tecnologia nuevaTecnologia) {
        return this.noTieneIncorporada(nuevaTecnologia) && this.incorporoRequisitosDe(nuevaTecnologia);
    }

    public Collection<Tecnologia> tecnologiasQueLeFaltanPara(Tecnologia tecnologia) {
        return tecnologia.getRequisitosDeIncorporacion().stream()
                .filter(this::noTieneIncorporada)
                .collect(Collectors.toList());
    }

    public Integer ingresosPorTurno() {
        return this.ciudades.stream()
                .mapToInt(Ciudad::ingresosPorTurno)
                .sum();
    }

    public Integer egresosPorTurno() {
        return this.ciudades.stream()
                .mapToInt(Ciudad::costoTotalMantenimientoEdificios)
                .sum();
    }

    public Integer potenciaTotalPorTurno() {
        return this.ciudades.stream()
                .mapToInt(Ciudad::potenciaTotalUnidadesMilitares)
                .sum();
    }

    private void pagarMantenimientoEdificios() {
        this.getTesoro().restarCantidadDePepines(this.egresosPorTurno());
    }

    private void dineroProducidoPorEdificiosEconomicos() {
        this.getTesoro().sumarCantidadDePepines(this.ingresosPorTurno());
    }

    public void evolucionPorTurno() {
        this.ciudades.forEach(Ciudad::aumentarPoblacion);
        this.pagarMantenimientoEdificios();
        this.dineroProducidoPorEdificiosEconomicos();
        this.getCiudades().forEach(Ciudad::crearYAgregarUnidadesMilitares);
        this.getCiudades().forEach(Ciudad::boostearEdificiosEconomicos);
        this.getCiudades().forEach(Ciudad::boostearEdificiosCulturales);
        this.getCiudades().forEach(Ciudad::boostearEdificiosMilitares);
    }

    public Collection<Ciudad> getCiudades() {
        return ciudades;
    }

    public void setCiudades(Collection<Ciudad> ciudades) {
        this.ciudades = ciudades;
    }

    public Collection<Tecnologia> getTecnologias() {
        return tecnologias;
    }

    public void setTecnologias(Collection<Tecnologia> tecnologias) {
        this.tecnologias = tecnologias;
    }

    public Tesoro getTesoro() {
        return tesoro;
    }

    public void setTesoro(Tesoro tesoro) {
        this.tesoro = tesoro;
    }

    public Humor getHumor() {
        return humor;
    }

    public void setHumor(Humor humor) {
        this.humor = humor;
    }

    public static Integer getFactorTranquilidad() {
        return factorTranquilidad;
    }

    public void setFactorTranquilidad(Integer factorTranquilidad) {
        this.factorTranquilidad = factorTranquilidad;
    }
}
