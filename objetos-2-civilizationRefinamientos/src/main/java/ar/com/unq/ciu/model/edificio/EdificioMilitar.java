package ar.com.unq.ciu.model.edificio;

import ar.com.unq.ciu.model.Boost;
import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.UnidadMilitar;

public class EdificioMilitar extends Edificio implements Boost {

    private Integer potenciaDeUnidadMilitar;

    public EdificioMilitar(Integer costoMantenimiento, Integer costoConstruccion, Ciudad ciudad, Integer potenciaDeUnidadMilitar) {
        super(costoMantenimiento, costoConstruccion, ciudad);
        this.potenciaDeUnidadMilitar = potenciaDeUnidadMilitar;
    }

    @Override
    public Integer cantidadDeCultura() {
        return 0;
    }

    @Override
    public Integer nivelDeTranquilidadGenerada() {
        return 1 + this.getCiudad().getImperio().getHumor().getNivelTranquilidad(this);
    }

    @Override
    public Integer valorDeEdificio() {
        return this.getPotenciaDeUnidadMilitar();
    }

    @Override
    public Boolean esDestacado() {
        return false;
    }

    @Override
    public boolean esCultural() {
        return false;
    }

    @Override
    public boolean esMilitar() {
        return true;
    }

    @Override
    public Integer getPotencia() {
        return this.potenciaDeUnidadMilitar;
    }

    public UnidadMilitar crearUnidadMilitar() {
        return new UnidadMilitar(this.potenciaDeUnidadMilitar);
    }

    public Integer getPotenciaDeUnidadMilitar() {
        return potenciaDeUnidadMilitar;
    }

    public void setPotenciaDeUnidadMilitar(Integer potenciaDeUnidadMilitar) {
        this.potenciaDeUnidadMilitar = potenciaDeUnidadMilitar;
    }

    @Override
    public void boostear() {
        this.potenciaDeUnidadMilitar += 5;
    }
}
