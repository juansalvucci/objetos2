package ar.com.unq.ciu.model.humor;

import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.edificio.Edificio;

public class Pacifista extends Humor {



    @Override
    public Integer getUnidadesDeDisconformidad(Ciudad ciudad) {
        return (ciudad.getCantidadDeHabitantes()/15000) + ciudad.getCantidadDeUnidadesMilitares();
    }

    @Override
    public Integer getNivelTranquilidad(Edificio edificio) {
        if (edificio.esCultural()) {
            return 15;
        } else if (edificio.esMilitar()) {
            return -3;
        } else {
            return 12;
        }
    }
}
