package ar.com.unq.ciu.model.edificio;

import ar.com.unq.ciu.model.Boost;
import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.Imperio;

public class EdificioCultural extends Edificio implements Boost {

    private Integer cantidadDeCultura;

    public EdificioCultural(Integer costoMantenimiento, Integer costoConstruccion, Ciudad ciudad, Integer cantidadDeCultura) {
        super(costoMantenimiento, costoConstruccion, ciudad);
        this.cantidadDeCultura = cantidadDeCultura;

    }

    @Override
    public Integer cantidadDeCultura() {
        return this.cantidadDeCultura;
    }

    @Override
    public Integer nivelDeTranquilidadGenerada() {
        return this.getCantidadDeCultura() / Imperio.getFactorTranquilidad() + this.getCiudad().getImperio().getHumor().getNivelTranquilidad(this);
    }

    @Override
    public Integer valorDeEdificio() {
        return this.cantidadDeCultura;
    }

    @Override
    public void boostear() {
        this.cantidadDeCultura = this.cantidadDeCultura * 2;
    }

    @Override
    public Boolean esDestacado() {
        return this.cantidadDeCultura > 100;
    }

    @Override
    public boolean esCultural() {
        return true;
    }

    @Override
    public boolean esMilitar() {
        return false;
    }

    public Integer getCantidadDeCultura() {
        return cantidadDeCultura;
    }

    public void setCantidadDeCultura(Integer cantidadDeCultura) {
        this.cantidadDeCultura = cantidadDeCultura;
    }
}
