package ar.com.unq.ciu.model;

import ar.com.unq.ciu.model.edificio.Edificio;
import ar.com.unq.ciu.model.edificio.EdificioCultural;
import ar.com.unq.ciu.model.edificio.EdificioEconomico;
import ar.com.unq.ciu.model.edificio.EdificioMilitar;
import ar.com.unq.ciu.model.estado.Convulsionada;
import ar.com.unq.ciu.model.estado.Estado;
import ar.com.unq.ciu.model.estado.Excitada;
import ar.com.unq.ciu.model.estado.Normal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.stream.Collectors;

public class Ciudad {

    private Integer cantidadDeHabitantes;
    private Imperio imperio;
    private Estado estado;
    private Collection<EdificioEconomico> edificiosEconomicos;
    private Collection<EdificioCultural> edificiosCulturales;
    private Collection<EdificioMilitar> edificiosMilitares;
    private Collection<UnidadMilitar> unidadesMilitares;

    public Ciudad(Integer cantidadDeHabitantes, Imperio imperio, Estado estado) {
        super();
        this.cantidadDeHabitantes = cantidadDeHabitantes;
        this.imperio = imperio;
        this.estado = estado;
        this.edificiosCulturales = new ArrayList<>();
        this.edificiosEconomicos = new ArrayList<>();
        this.unidadesMilitares = new ArrayList<>();
        this.edificiosMilitares = new ArrayList<>();
    }

    public Collection<Edificio> getEdificios() {
        Collection<Edificio> edificios = new ArrayList<>();
        edificios.addAll(this.getEdificiosEconomicos());
        edificios.addAll(this.getEdificiosCulturales());
        edificios.addAll(this.getEdificiosMilitares());
        return edificios;
    }

    public void potenciarse() {
        if (this.estado.getDescripcion().equals("Normal")) {
            this.setEstado(Excitada.getInstance());
        }
        if (this.estado.getDescripcion().equals("Excitada")) {
            this.setEstado(Excitada.getInstance());
        }
        if (this.estado.getDescripcion().equals("Convulsionada")) {
            this.setEstado(Normal.getInstance());
        }
    }

    public void complicarse() {
        if (this.estado.getDescripcion().equals("Normal")) {
            this.setEstado(Convulsionada.getInstance());
        }
        if (this.estado.getDescripcion().equals("Excitada")) {
            this.setEstado(Normal.getInstance());
        }
        if (this.estado.getDescripcion().equals("Convulsionada")) {
            this.setEstado(Convulsionada.getInstance());
        }
    }

    public void agregarEdificio(EdificioCultural edificioCultural) {
        this.getEdificiosCulturales().add(edificioCultural);
    }

    public void agregarEdificio(EdificioEconomico edificioEconomico) {
        this.getEdificiosEconomicos().add(edificioEconomico);
    }

    public void agregarEdificio(EdificioMilitar edificioMilitar) {
        this.getEdificiosMilitares().add(edificioMilitar);
    }

    public Integer getCantidadDeCulturaDeEdificios() {
        return this.getEdificios().stream()
                .mapToInt(Edificio::cantidadDeCultura)
                .sum();
    }

    public Integer getCantidadDeCulturaDeTecnologias() {
        return this.imperio.getTecnologias().stream()
                .mapToInt(Tecnologia::getCantidadCulturaQueAporta)
                .sum();
    }

    public Integer getCantidadDeCultura() {
        return this.getCantidadDeCulturaDeEdificios() + this.getCantidadDeCulturaDeTecnologias();
    }

    public Integer getNivelTranquilidadGeneradoPorEdificios() {
        return this.getEdificios().stream()
                .mapToInt(Edificio::nivelDeTranquilidadGenerada)
                .sum();
    }

    public Integer getNivelDeDisconformidad() {
        return this.imperio.getHumor().getUnidadesDeDisconformidad(this);
    }

    public void aumentarPoblacion() {
        Integer valorAumentoPoblacion = this.estado.getPorcentajeAumentoPoblacion(this);

        this.cantidadDeHabitantes += valorAumentoPoblacion;
    }

    public void crearYAgregarUnidadesMilitares() {
        Collection<UnidadMilitar> unidades = this.getEdificiosMilitares().stream()
                .map(EdificioMilitar::crearUnidadMilitar)
                .collect(Collectors.toList());

        this.getUnidadesMilitares().addAll(unidades);
    }

    public void agregarUnidadMilitar(UnidadMilitar unidadMilitar) {
        this.unidadesMilitares.add(unidadMilitar);
    }

    public void boostearEdificiosCulturales() {
        this.estado.boostearEdificioCultural(this);
    }

    public void boostearEdificiosEconomicos() {
        this.estado.boostearEdificioEconomico(this);
    }

    public void boostearEdificiosMilitares() {
        this.edificiosMilitares.forEach(edificioMilitar -> estado.boostearEdificioMilitar(this));
    }

    public EdificioCultural edificioCulturalQueMasIrradia() {
        return this.edificiosCulturales.stream()
                .max(Comparator.comparing(EdificioCultural::cantidadDeCultura)).get();
    }

    public Integer getCantidadDeUnidadesMilitares() {
        return this.unidadesMilitares.size();
    }

    public Integer getCantidadDeHabitantes() {
        return cantidadDeHabitantes;
    }

    public void setCantidadDeHabitantes(Integer cantidadDeHabitantes) {
        this.cantidadDeHabitantes = cantidadDeHabitantes;
    }

    public Collection<EdificioCultural> getEdificiosCulturales() {
        return this.edificiosCulturales;
    }

    public Integer getCantidadDeEdificiosCulturales() {
        return this.getEdificiosCulturales().size();
    }

    public Collection<EdificioEconomico> getEdificiosEconomicos() {
        return edificiosEconomicos;
    }

    public Collection<EdificioMilitar> getEdificiosMilitares() {
        return edificiosMilitares;
    }

    public void setEdificiosMilitares(Collection<EdificioMilitar> edificiosMilitares) {
        this.edificiosMilitares = edificiosMilitares;
    }

    public Integer ingresosPorTurno() {
        return this.estado.aporteAlTesoro(this);
    }

    public Integer costoTotalMantenimientoEdificios() {
        return this.estado.getCostoDeMantenimiento(this);
    }

    public Integer potenciaTotalUnidadesMilitares() {
        return this.getUnidadesMilitares().stream()
                .mapToInt(UnidadMilitar::getPotencia)
                .sum();
    }

    public Collection<UnidadMilitar> getUnidadesMilitares() {
        return unidadesMilitares;
    }

    public void setUnidadesMilitares(Collection<UnidadMilitar> unidadesMilitares) {
        this.unidadesMilitares = unidadesMilitares;
    }

    public Integer cantidadEdificiosDestacados() {
        return (int) this.getEdificios().stream()
                .filter(Edificio::esDestacado).count();
    }

    public Imperio getImperio() {
        return imperio;
    }

    public void setImperio(Imperio imperio) {
        this.imperio = imperio;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public void quitarEdificio(Edificio edificio) {
        this.getEdificios().remove(edificio);
    }
}
