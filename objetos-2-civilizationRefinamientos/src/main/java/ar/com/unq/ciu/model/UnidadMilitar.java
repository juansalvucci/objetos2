package ar.com.unq.ciu.model;

public class UnidadMilitar {

    private Integer potencia;

    public UnidadMilitar(Integer potencia) {
        this.potencia = potencia;
    }

    public Integer getPotencia() {
        return potencia;
    }

    public void setPotencia(Integer potencia) {
        this.potencia = potencia;
    }
}
