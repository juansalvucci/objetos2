package ar.com.unq.ciu.model.estado;

import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.edificio.Edificio;
import ar.com.unq.ciu.model.edificio.EdificioEconomico;

public final class Normal extends Estado {

    private static Estado instance;

    public Normal() {
        super();
        this.descripcion = "Normal";
    }

    public static Estado getInstance() {
        if (instance==null)
            instance = new Normal();
        return instance;
    }

    @Override
    public Integer getCostoDeMantenimiento(Ciudad ciudad) {
        return ciudad.getEdificios().stream()
                .mapToInt(Edificio::getCostoMantenimiento)
                .sum();
    }

    @Override
    public Integer aporteAlTesoro(Ciudad ciudad) {
        return ciudad.getEdificiosEconomicos().stream()
                .mapToInt(EdificioEconomico::getCantidadDineroProducido)
                .sum();
    }

    private Integer generadorAleatorio() {
        return (int)(Math.random() * 100);
    }

    @Override
    public void boostearEdificioEconomico(Ciudad ciudad) {
        Integer probabilidad = 20 + (ciudad.cantidadEdificiosDestacados() * 10);

        if (probabilidad >= 100) {
            ciudad.getEdificiosEconomicos().stream()
                    .findAny().get().boostear();
        } else if (this.generadorAleatorio() < probabilidad) {
            ciudad.getEdificiosEconomicos().stream()
                    .findAny().get().boostear();
        }
    }

    @Override
    public void boostearEdificioCultural(Ciudad ciudad) {
        Integer probabilidad = 20 + (ciudad.cantidadEdificiosDestacados() * 10);

        if (probabilidad >= 100) {
            ciudad.getEdificiosCulturales().stream()
                    .findAny().get().boostear();
        } else if (this.generadorAleatorio() < probabilidad) {
            ciudad.getEdificiosCulturales().stream()
                    .findAny().get().boostear();
        }
    }

    @Override
    public Boolean esFeliz(Ciudad ciudad) {
        return ciudad.getNivelTranquilidadGeneradoPorEdificios() > ciudad.getNivelDeDisconformidad();
    }

    @Override
    public void boostearEdificioMilitar(Ciudad ciudad) {
        ciudad.getEdificiosMilitares().stream()
                .findAny().get().boostear();
    }
}
