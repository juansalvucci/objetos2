package ar.com.unq.ciu.model.humor;

import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.edificio.Edificio;

public class Perseguida extends Humor {

    @Override
    public Integer getUnidadesDeDisconformidad(Ciudad ciudad) {
        if (ciudad.getCantidadDeUnidadesMilitares().equals(0)) {
            return 13 + (ciudad.getCantidadDeHabitantes()/40000);
        } else if (ciudad.getCantidadDeUnidadesMilitares()>=1 && ciudad.getCantidadDeUnidadesMilitares()<=3) {
            return 8 + (ciudad.getCantidadDeHabitantes()/40000);
        } else {
            return 3 + (ciudad.getCantidadDeHabitantes()/40000);
        }
    }

    @Override
    public Integer getNivelTranquilidad(Edificio edificio) {
        if (edificio.esCultural()) {
            return 1;
        } else if (edificio.esMilitar()) {
            return edificio.getPotencia() * 3;
        } else {
            if (edificio.getPepinesPorTurno() <= 500) {
                return 10;
            } else {
                return 15;
            }
        }
    }
}
