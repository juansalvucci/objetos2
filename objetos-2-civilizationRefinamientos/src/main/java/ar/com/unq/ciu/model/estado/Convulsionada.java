package ar.com.unq.ciu.model.estado;

import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.edificio.Edificio;

public final class Convulsionada extends Estado {

    private static Estado instance;

    public Convulsionada() {
        super();
        this.descripcion = "Convulsionada";
    }

    public static Estado getInstance() {
        if (instance==null)
            instance = new Convulsionada();
        return instance;
    }

    @Override
    public Integer getCostoDeMantenimiento(Ciudad ciudad) {
        return ciudad.getEdificios().stream()
                .mapToInt(Edificio::getCostoMantenimiento)
                .sum();
    }

    @Override
    public Integer aporteAlTesoro(Ciudad ciudad) {
        return 0;
    }

    @Override
    public void boostearEdificioEconomico(Ciudad ciudad) {

    }

    @Override
    public void boostearEdificioCultural(Ciudad ciudad) {

    }

    @Override
    public Boolean esFeliz(Ciudad ciudad) {
        return ciudad.potenciaTotalUnidadesMilitares() >= 100;
    }

    @Override
    public void boostearEdificioMilitar(Ciudad ciudad) {

    }
}
