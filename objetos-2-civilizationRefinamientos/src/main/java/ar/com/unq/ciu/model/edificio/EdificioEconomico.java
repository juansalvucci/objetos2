package ar.com.unq.ciu.model.edificio;

import ar.com.unq.ciu.model.Boost;
import ar.com.unq.ciu.model.Ciudad;

public class EdificioEconomico extends Edificio implements Boost {

    private Integer cantidadDineroQueProduce;

    public EdificioEconomico(Integer costoMantenimiento, Integer costoConstruccion, Ciudad ciudad, Integer cantidadDinero) {
        super(costoMantenimiento, costoConstruccion, ciudad);
        this.cantidadDineroQueProduce = cantidadDinero;
    }

    @Override
    public Integer cantidadDeCultura() {
        if (this.cantidadDineroQueProduce <= 500) {
            return 2;
        } else {
            return 3;
        }
    }

    @Override
    public Integer nivelDeTranquilidadGenerada() {
        return this.getCiudad().getImperio().getHumor().getNivelTranquilidad(this);
    }

    @Override
    public Integer valorDeEdificio() {
        return this.getCantidadDineroProducido();
    }

    public Integer getCantidadDineroProducido() {
        return cantidadDineroQueProduce;
    }

    public void setCantidadDineroProducida(Integer cantidadDineroProducida) {
        this.cantidadDineroQueProduce = cantidadDineroProducida;
    }

    @Override
    public void boostear() {
        this.cantidadDineroQueProduce = this.cantidadDineroQueProduce * 2;
    }


    @Override
    public Boolean esDestacado() {
        return this.cantidadDineroQueProduce > 200;
    }

    @Override
    public boolean esCultural() {
        return false;
    }

    @Override
    public boolean esMilitar() {
        return false;
    }

    @Override
    public Integer getPepinesPorTurno() {
        return this.cantidadDineroQueProduce;
    }
}
