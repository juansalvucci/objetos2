package ar.com.unq.ciu.model.edificio;

import ar.com.unq.ciu.model.Ciudad;

public abstract class Edificio {

    private Integer costoMantenimiento;
    private Integer costoConstruccion;
    private Ciudad ciudad;

    public Edificio(Integer costoMantenimiento, Integer costoConstruccion, Ciudad ciudad) {
        this.costoMantenimiento = costoMantenimiento;
        this.costoConstruccion = costoConstruccion;
        this.ciudad = ciudad;
    }

    public abstract Integer cantidadDeCultura();

    public abstract Integer nivelDeTranquilidadGenerada();

    public abstract Integer valorDeEdificio();

    public Integer getCostoMantenimiento() {
        return costoMantenimiento;
    }

    public abstract Boolean esDestacado();

    public void setCostoMantenimiento(Integer costoMantenimiento) {
        this.costoMantenimiento = costoMantenimiento;
    }

    public Integer getCostoConstruccion() {
        return costoConstruccion;
    }

    public void setCostoConstruccion(Integer costoConstruccion) {
        this.costoConstruccion = costoConstruccion;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public abstract boolean esCultural();

    public abstract boolean esMilitar();

    public Integer getPotencia() {
        return 0;
    }

    public Integer getPepinesPorTurno() {
        return 0;
    }
}
