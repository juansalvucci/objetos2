package ar.com.unq.ciu.model.humor;

import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.edificio.Edificio;

public abstract class Humor {

    public abstract Integer getUnidadesDeDisconformidad(Ciudad ciudad);

    public abstract Integer getNivelTranquilidad(Edificio edificio);

}
