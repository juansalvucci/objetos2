package ar.com.unq.ciu.model.estado;

import ar.com.unq.ciu.model.Ciudad;

public abstract class Estado {

    protected String descripcion;

    public Estado() {
        super();
    }

    public Integer getPorcentajeAumentoPoblacion(Ciudad ciudad) {
        return ciudad.getCantidadDeHabitantes() * 5/100;
    };

    public abstract Integer getCostoDeMantenimiento(Ciudad ciudad);

    public abstract Integer aporteAlTesoro(Ciudad ciudad);

    public abstract void boostearEdificioEconomico(Ciudad ciudad);

    public abstract void boostearEdificioCultural(Ciudad ciudad);

    public abstract Boolean esFeliz(Ciudad ciudad);

    public abstract void boostearEdificioMilitar(Ciudad ciudad);

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
