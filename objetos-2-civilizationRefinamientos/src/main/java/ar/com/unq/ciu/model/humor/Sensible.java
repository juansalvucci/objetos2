package ar.com.unq.ciu.model.humor;

import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.Imperio;
import ar.com.unq.ciu.model.edificio.Edificio;

public class Sensible extends Humor {

    @Override
    public Integer getUnidadesDeDisconformidad(Ciudad ciudad) {
        if (ciudad.getCantidadDeHabitantes() <=200000) {
            return (ciudad.getCantidadDeHabitantes()/20000) - ciudad.getCantidadDeEdificiosCulturales();
        } else {
            return (10 + (200000 - ciudad.getCantidadDeHabitantes())/40000) - ciudad.getCantidadDeEdificiosCulturales();
        }
    }

    @Override
    public Integer getNivelTranquilidad(Edificio edificio) {
        if (edificio.esCultural()) {
            return edificio.cantidadDeCultura()/ Imperio.getFactorTranquilidad();
        } else if (edificio.esMilitar()) {
            return 0;
        } else {
            return 6;
        }
    }
}
