package ar.com.unq.ciu.model;

import java.util.ArrayList;
import java.util.Collection;

public class Tecnologia {

    private String nombre;
    private Integer cantidadCulturaQueAporta;
    private Collection<Tecnologia> requisitosDeIncorporacion;

    public Tecnologia(String nombre, Integer cantidadCulturaQueAporta) {
        super();
        this.nombre = nombre;
        this.cantidadCulturaQueAporta = cantidadCulturaQueAporta;

        this.requisitosDeIncorporacion = new ArrayList<>();
    }

    public void agregarRequisito(Tecnologia tecnologia) {
        this.getRequisitosDeIncorporacion().add(tecnologia);
    }

    public Collection<Tecnologia> getRequisitosDeIncorporacion() {
        return this.requisitosDeIncorporacion;
    }

    public void setRequisitosDeIncorporacion(Collection<Tecnologia> requisitosDeIncorporacion) {
        this.requisitosDeIncorporacion = requisitosDeIncorporacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCantidadCulturaQueAporta() {
        return cantidadCulturaQueAporta;
    }

    public void setCantidadCulturaQueAporta(Integer cantidadCulturaQueAporta) {
        this.cantidadCulturaQueAporta = cantidadCulturaQueAporta;
    }
}
