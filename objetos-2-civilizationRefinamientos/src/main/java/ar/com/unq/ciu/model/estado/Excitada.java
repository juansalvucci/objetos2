package ar.com.unq.ciu.model.estado;

import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.edificio.Edificio;
import ar.com.unq.ciu.model.edificio.EdificioCultural;
import ar.com.unq.ciu.model.edificio.EdificioEconomico;

import java.util.*;
import java.util.stream.Collectors;

public final class Excitada extends Estado {

    private static Estado instance;

    public Excitada() {
        super();
        this.descripcion = "Excitada";
    }

    public static Estado getInstance() {
        if (instance==null)
            instance = new Excitada();
        return instance;
    }

    @Override
    public Integer getPorcentajeAumentoPoblacion(Ciudad ciudad) {
        if (this.esFeliz(ciudad)) {
            return 13;
        } else {
            return 5;
        }
    }

    private Collection<Edificio> getEdificiosQuePaganMantenimiento(Ciudad ciudad) {
        Collection<Edificio> edificios = new ArrayList<>();
        edificios.addAll(ciudad.getEdificiosEconomicos());
        edificios.addAll(ciudad.getEdificiosMilitares());
        return edificios;
    }

    @Override
    public Integer getCostoDeMantenimiento(Ciudad ciudad) {
        return Objects.requireNonNull(this.getEdificiosQuePaganMantenimiento(ciudad)).stream()
                .mapToInt(Edificio::getCostoMantenimiento)
                .sum();
    }

    @Override
    public Integer aporteAlTesoro(Ciudad ciudad) {
        Integer ingresosEdificiosEconomicos = ciudad.getEdificiosEconomicos().stream()
                .mapToInt(EdificioEconomico::getCantidadDineroProducido)
                .sum();

        return ingresosEdificiosEconomicos + ciudad.edificioCulturalQueMasIrradia().getCantidadDeCultura();
    }

    private List<EdificioEconomico> getEdificiosEconomicosQueMenosAportan(Ciudad ciudad) {
        return ciudad.getEdificiosEconomicos().stream()
                .sorted(Comparator.comparing(EdificioEconomico::getCantidadDineroProducido))
                .collect(Collectors.toList());
    }

    private List<EdificioCultural> getEdificiosCulturalesQueMenosIrradian(Ciudad ciudad) {
        return ciudad.getEdificiosCulturales().stream()
                .sorted(Comparator.comparing(EdificioCultural::cantidadDeCultura))
                .collect(Collectors.toList());
    }

    @Override
    public void boostearEdificioEconomico(Ciudad ciudad) {
        ArrayList<EdificioEconomico> lista = new ArrayList<>();
        lista.addAll(this.getEdificiosEconomicosQueMenosAportan(ciudad));

        lista.get(0).boostear();
        lista.get(1).boostear();
    }

    @Override
    public void boostearEdificioCultural(Ciudad ciudad) {
        ArrayList<EdificioCultural> lista = new ArrayList<>();
        lista.addAll(this.getEdificiosCulturalesQueMenosIrradian(ciudad));

        lista.get(0).boostear();
        lista.get(1).boostear();
    }

    @Override
    public void boostearEdificioMilitar(Ciudad ciudad) {
        ciudad.getEdificiosMilitares().stream()
                .findAny().get().boostear();
    }

    @Override
    public Boolean esFeliz(Ciudad ciudad) {
        return ciudad.getCantidadDeCulturaDeEdificios() > 80;
    }
}
