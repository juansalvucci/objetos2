package ar.com.unq.ciu.test;

import ar.com.unq.ciu.model.Ciudad;
import ar.com.unq.ciu.model.Imperio;
import ar.com.unq.ciu.model.Tecnologia;
import ar.com.unq.ciu.model.Tesoro;
import ar.com.unq.ciu.model.edificio.EdificioCultural;
import ar.com.unq.ciu.model.edificio.EdificioEconomico;
import ar.com.unq.ciu.model.edificio.EdificioMilitar;
import ar.com.unq.ciu.model.estado.Convulsionada;
import ar.com.unq.ciu.model.estado.Excitada;
import ar.com.unq.ciu.model.estado.Normal;
import ar.com.unq.ciu.model.humor.Pacifista;
import ar.com.unq.ciu.model.humor.Perseguida;
import ar.com.unq.ciu.model.humor.Sensible;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CivilizationRefinamientosTest {

    Tesoro tesoroImperio1 = new Tesoro(5000);
    Tesoro tesoroImperio2 = new Tesoro(5000);
    Tesoro tesoroImperio3 = new Tesoro(5000);

    Pacifista humorPacifista = new Pacifista();
    Perseguida humorPerseguida = new Perseguida();
    Sensible humorSensible = new Sensible();

    Imperio imperio1 = new Imperio(tesoroImperio1, 10, humorPacifista);
    Imperio imperio2 = new Imperio(tesoroImperio2, 20, humorSensible);
    Imperio imperio3 = new Imperio(tesoroImperio3, 30, humorPerseguida);

    Ciudad ciudad1 = new Ciudad(500, imperio1, Convulsionada.getInstance());
    Ciudad ciudad2 = new Ciudad(150000, imperio1, Excitada.getInstance());
    Ciudad ciudad3 = new Ciudad(1500, imperio1, Normal.getInstance());
    Ciudad ciudad4 = new Ciudad(200, imperio2, Excitada.getInstance());
    Ciudad ciudad5 = new Ciudad(20000, imperio3, Normal.getInstance());
    Ciudad ciudad6 = new Ciudad(10000, imperio2, Normal.getInstance());

    EdificioEconomico edificioCE1 = new EdificioEconomico(200, 10000, ciudad1, 30000);
    EdificioEconomico edificioCE2 = new EdificioEconomico(300, 10100, ciudad2, 50000);
    EdificioEconomico edificioCE38 = new EdificioEconomico(300, 10100, ciudad2, 20000);
    EdificioEconomico edificioCE387 = new EdificioEconomico(300, 10100, ciudad2, 10000);
    EdificioEconomico edificioCE3 = new EdificioEconomico(350, 14000, ciudad3, 40000);
    EdificioEconomico edificioCE32 = new EdificioEconomico(350, 14000, ciudad3, 10000);
    EdificioEconomico edificioCE4 = new EdificioEconomico(200, 10000, ciudad4, 50000);
    EdificioEconomico edificioCE5 = new EdificioEconomico(15000, 10000, ciudad5, 5000);
    EdificioEconomico edEC6A = new EdificioEconomico(1000, 1000, ciudad6, 1000);
    EdificioEconomico edEC6B = new EdificioEconomico(2000, 2000, ciudad6, 2000);
    EdificioEconomico edEC6C = new EdificioEconomico(3000, 3000, ciudad6, 3000);
    EdificioEconomico edEC6D = new EdificioEconomico(3000, 3000, ciudad6, 4000);

    EdificioCultural edificioCC1 = new EdificioCultural(500, 20000, ciudad1, 10);
    EdificioCultural edificioCC2 = new EdificioCultural(600, 20100, ciudad2, 10);
    EdificioCultural edificioCC4 = new EdificioCultural(600, 20100, ciudad2, 20);
    EdificioCultural edificioCC3 = new EdificioCultural(550, 13000, ciudad3, 150);
    EdificioCultural edificioCC44 = new EdificioCultural(500, 20000, ciudad4, 5);
    EdificioCultural edificioCC32 = new EdificioCultural(550, 13000, ciudad3, 5);
    EdificioCultural edificioCC45 = new EdificioCultural(5000, 15000, ciudad5, 50);
    EdificioCultural edCC6A = new EdificioCultural(5000, 15000, ciudad6, 150);
    EdificioCultural edCC6B = new EdificioCultural(5000, 15000, ciudad6, 250);
    EdificioCultural edCC6C = new EdificioCultural(5000, 15000, ciudad6, 350);
    EdificioCultural edCC6D = new EdificioCultural(5000, 15000, ciudad6, 450);

    EdificioMilitar edificioCM1 = new EdificioMilitar(300, 15000, ciudad1, 5);
    EdificioMilitar edificioCM2 = new EdificioMilitar(700, 15100, ciudad2, 50);
    EdificioMilitar edificioCM22 = new EdificioMilitar(700, 15100, ciudad2, 25);
    EdificioMilitar edificioCM23 = new EdificioMilitar(700, 15100, ciudad2, 25);
    EdificioMilitar edificioCM3 = new EdificioMilitar(800, 16000, ciudad3, 5);
    EdificioMilitar edificioCM4 = new EdificioMilitar(800, 16000, ciudad4, 5);
    EdificioMilitar edificioCM5 = new EdificioMilitar(12000, 16000, ciudad5, 30);
    EdificioMilitar edMC6A = new EdificioMilitar(12000, 16000, ciudad6, 30);

    Tecnologia tecnologia1 = new Tecnologia("Tecnologia 1", 20);

    //requisitos tecnologia 1:
    Tecnologia tecnologia2 = new Tecnologia("Tecnologia 2", 30);
    Tecnologia tecnologia3 = new Tecnologia("Tecnologia 3", 50);

    @Test
    public void test1() {
        System.out.println("===============================================================================================");
        System.out.println("ESTADO: ");

        ciudad2.agregarEdificio(edificioCE2);
        ciudad2.agregarEdificio(edificioCE38);
        ciudad2.agregarEdificio(edificioCE387);
        ciudad2.agregarEdificio(edificioCC2);
        ciudad2.agregarEdificio(edificioCC4);
        ciudad2.agregarEdificio(edificioCM2);
        ciudad2.agregarEdificio(edificioCM22);
        ciudad2.agregarEdificio(edificioCM23);

        ciudad1.getEdificios();
        ciudad2.getEdificios();
        ciudad3.getEdificios();
        ciudad4.getEdificios();
        ciudad5.getEdificios();
        ciudad6.getEdificios();

        imperio1.agregarCiudad(ciudad1);
        imperio1.agregarCiudad(ciudad2);
        imperio1.agregarCiudad(ciudad3);

        //inciso a
        ciudad2.aumentarPoblacion();
        Assertions.assertEquals((Integer) 150005, ciudad2.getCantidadDeHabitantes());

        //inciso b
        Assertions.assertEquals((Integer) 3000, ciudad2.costoTotalMantenimientoEdificios());

        //inciso c
        Assertions.assertEquals((Integer) 80020, ciudad2.ingresosPorTurno());

        //inciso e
        ciudad2.boostearEdificiosCulturales();
        Assertions.assertEquals((Integer) 20, edificioCC2.getCantidadDeCultura());
        Assertions.assertEquals((Integer) 40, edificioCC4.getCantidadDeCultura());

        ciudad2.boostearEdificiosEconomicos();
        Assertions.assertEquals((Integer) 20000, edificioCE387.getCantidadDineroProducido());
        Assertions.assertEquals((Integer) 40000, edificioCE38.getCantidadDineroProducido());

        //INCISO G
        ciudad2.boostearEdificiosMilitares();
        System.out.println(edificioCM2.getPotencia());
        System.out.println(edificioCM22.getPotencia());
        System.out.println(edificioCM23.getPotencia());
    }

    @Test
    public void test2() {
        System.out.println("===============================================================================================");
        System.out.println("Testeo de mensaje complicarse y potenciarse");

        ciudad1.setEstado(Convulsionada.getInstance());
        System.out.println("Aca deberia mantener el estado  Normal");
        ciudad1.potenciarse();
        System.out.println(ciudad1.getEstado().getDescripcion());

        ciudad1.setEstado(Convulsionada.getInstance());
        System.out.println("Aca deberia mantener en Convulsionada");
        ciudad1.complicarse();
        System.out.println(ciudad1.getEstado().getDescripcion());

        ciudad1.setEstado(Excitada.getInstance());
        System.out.println("Aca deberia mantener el estado  Excitada");
        ciudad1.potenciarse();
        System.out.println(ciudad1.getEstado().getDescripcion());

        ciudad1.setEstado(Excitada.getInstance());
        System.out.println("Aca deberia pasar a Normal");
        ciudad1.complicarse();
        System.out.println(ciudad1.getEstado().getDescripcion());

        ciudad1.setEstado(Normal.getInstance());
        System.out.println("Aca deberia pasar al estado  Excitada");
        ciudad1.potenciarse();
        System.out.println(ciudad1.getEstado().getDescripcion());

        ciudad1.setEstado(Normal.getInstance());
        System.out.println("Aca deberia pasar al estado  convulsionada");
        ciudad1.complicarse();
        System.out.println(ciudad1.getEstado().getDescripcion());
    }

    @Test
    public void test3() {
        System.out.println("===============================================================================================");
        System.out.println("TRANQUILIDAD: ");

        ciudad4.agregarEdificio(edificioCE4);
        ciudad4.agregarEdificio(edificioCC44);
        ciudad4.agregarEdificio(edificioCM4);

        ciudad4.getEdificios();

        imperio2.agregarCiudad(ciudad4);
        Assertions.assertEquals((Integer) 7, ciudad4.getNivelTranquilidadGeneradoPorEdificios());
    }

    @Test
    public void test4() {
        System.out.println("===============================================================================================");
        System.out.println("DISCONFORMIDAD: ");

        ciudad2.agregarUnidadMilitar(edificioCM2.crearUnidadMilitar());
        ciudad2.agregarUnidadMilitar(edificioCM22.crearUnidadMilitar());

        Assertions.assertEquals((Integer) 12, ciudad2.getNivelDeDisconformidad());
    }

    @Test
    public void test5() {
        System.out.println("===============================================================================================");
        System.out.println("BOOSTEO: ");

        ciudad6.agregarEdificio(edEC6A);
        ciudad6.agregarEdificio(edEC6B);
        ciudad6.agregarEdificio(edEC6C);
        ciudad6.agregarEdificio(edEC6D);
        ciudad6.agregarEdificio(edCC6A);
        ciudad6.agregarEdificio(edCC6B);
        ciudad6.agregarEdificio(edCC6C);
        ciudad6.agregarEdificio(edCC6D);
        ciudad6.agregarEdificio(edMC6A);

        ciudad6.getEdificios();
        imperio2.agregarCiudad(ciudad6);

        ciudad6.boostearEdificiosEconomicos();
        System.out.println("ECONOMICOS: ");
        ciudad6.getEdificiosEconomicos().forEach(e -> System.out.println(e.getCantidadDineroProducido()));

        ciudad6.boostearEdificiosCulturales();
        System.out.println("CULTURALES: ");
        ciudad6.getEdificiosCulturales().forEach(e -> System.out.println(e.cantidadDeCultura()));

        ciudad6.boostearEdificiosMilitares();
        System.out.println("MILITARES: ");
        ciudad6.getEdificiosMilitares().forEach(e -> System.out.println(e.getPotencia()));
    }

    @Test
    public void test6() {
        System.out.println("===============================================================================================");
        System.out.println("HUMOR: ");

        ciudad1.agregarEdificio(edificioCE1);
        ciudad1.agregarEdificio(edificioCC1);
        ciudad1.agregarEdificio(edificioCM1);
        ciudad3.agregarEdificio(edificioCE3);
        ciudad3.agregarEdificio(edificioCC3);
        ciudad3.agregarEdificio(edificioCM3);
        ciudad5.agregarEdificio(edificioCE5);
        ciudad5.agregarEdificio(edificioCC45);
        ciudad5.agregarEdificio(edificioCM5);
        ciudad4.agregarEdificio(edificioCE4);
        ciudad4.agregarEdificio(edificioCC44);
        ciudad4.agregarEdificio(edificioCM4);

        ciudad4.getEdificios();

        ciudad1.getEdificios();
        ciudad3.getEdificios();
        ciudad4.getEdificios();
        ciudad5.getEdificios();

        imperio3.agregarCiudad(ciudad5);

        System.out.println("imperio con humor pacifista");
        Assertions.assertEquals(25, ciudad1.getNivelTranquilidadGeneradoPorEdificios());

        System.out.println("imperio con humor sensible");
        Assertions.assertEquals(7, ciudad4.getNivelTranquilidadGeneradoPorEdificios());

        System.out.println("imperio con humor perseguida");
        Assertions.assertEquals(108, ciudad5.getNivelTranquilidadGeneradoPorEdificios());
    }

    @Test
    public void test7() {
        System.out.println("===============================================================================================");
        System.out.println("TECNOLOGIAS: ");

        System.out.println("Aca deberiamos obtener una lista vacía");
        System.out.println(imperio1.tecnologiasQueLeFaltanPara(tecnologia1));

        tecnologia1.agregarRequisito(tecnologia2);
        tecnologia1.agregarRequisito(tecnologia3);

        System.out.println("Aca deberiamos obtener la tecnologia 2 y tecnologia 3");
        imperio1.tecnologiasQueLeFaltanPara(tecnologia1).forEach(t -> System.out.println(t.getNombre()));

        imperio1.agregarTecnologia(tecnologia2);

        System.out.println("Aca deberiamos obtener la tecnologia 3");
        imperio1.tecnologiasQueLeFaltanPara(tecnologia1).forEach(t -> System.out.println(t.getNombre()));
    }

    @Test
    public void test8() {
        System.out.println("===============================================================================================");
        System.out.println("CULTURA: ");

        ciudad1.agregarEdificio(edificioCE1);
        ciudad1.agregarEdificio(edificioCC1);
        ciudad1.agregarEdificio(edificioCM1);

        ciudad1.getEdificios();

        imperio1.agregarTecnologia(tecnologia3);
        imperio1.agregarTecnologia(tecnologia2);
        imperio1.agregarTecnologia(tecnologia1);

        Assertions.assertEquals((Integer) 113, ciudad1.getCantidadDeCultura());
    }
}