package ar.com.unq.ciu.test;

import ar.com.unq.ciu.model.*;
import junit.framework.TestCase;

import java.util.Arrays;

public class Tests extends TestCase {

    public void test1() {
        // CERVEZAS:
        MarcaDeCerveza warsteiner = new MarcaDeCervezaRubia(3.0, "Alemania", 4.8);
        MarcaDeCerveza Krombacher = new MarcaDeCervezaRubia(10.0, "Alemania", 5.3);
        MarcaDeCerveza stellaArtois = new MarcaDeCervezaNegra(5.0, "Belgica");
        MarcaDeCerveza achel = new MarcaDeCervezaNegra(12.0, "Belgica");
        MarcaDeCerveza pilsnerUrquell = new MarcaDeCervezaRoja(8.0, "Republica Checa");
        MarcaDeCerveza staropramen = new MarcaDeCervezaRoja(5.0, "Republica Checa");

        // CARPAS:
        Carpa carpa1 = new Carpa(10, true, warsteiner);
        Carpa carpa2 = new Carpa(50, true, Krombacher);
        Carpa carpa3 = new Carpa(30, false, stellaArtois);
        Carpa carpa4 = new Carpa(40, false, achel);
        Carpa carpa5 = new Carpa(40, false, pilsnerUrquell);
        Carpa carpa6 = new Carpa(10, false, staropramen);

        // JARRAS:
        Jarra jarra1 = new Jarra(0.5, carpa1);
        Jarra jarra2 = new Jarra(1.0, carpa1);
        Jarra jarra3 = new Jarra(0.5, carpa2);
        Jarra jarra4 = new Jarra(1.0, carpa2);
        Jarra jarra5 = new Jarra(0.5, carpa3);
        Jarra jarra6 = new Jarra(1.0, carpa3);
        Jarra jarra7 = new Jarra(0.5, carpa4);
        Jarra jarra8 = new Jarra(1.0, carpa4);
        Jarra jarra9 = new Jarra(0.5, carpa5);
        Jarra jarra10 = new Jarra(1.0, carpa5);
        Jarra jarra11 = new Jarra(0.5, carpa6);
        Jarra jarra12 = new Jarra(1.0, carpa6);

        // PERSONAS:
        Persona richard = new PersonaAlemana(80, true, 2);
        Persona till = new PersonaAlemana(95, true, 5);
        Persona vanDamme = new PersonaBelga(74, false, 8);
        Persona dirk = new PersonaBelga(85, true, 10);
        Persona berdych = new PersonaCheca(100, false, 4);
        Persona navratilova = new PersonaCheca(75, false, 2);
        Persona christoph = new PersonaAlemana(85, false, 4);
        Persona oliver = new PersonaBelga(80, false, 7);


        // PUNTO 1:
        assertEquals(0.024, jarra1.cantidadDeAlcohol());
        assertEquals(0.025, jarra5.cantidadDeAlcohol());
        assertEquals(0.0625, jarra10.cantidadDeAlcohol());

        // PUNTO 2:
        carpa1.ingresar(richard);
        richard.comprarJarra(jarra1);
        richard.comprarJarra(jarra1);

        assertEquals(0.048, richard.alcoholIngerido());

        // PUNTO 3:
        carpa2.ingresar(till);
        till.comprarJarra(jarra4);
        till.comprarJarra(jarra3);

        assertEquals(Boolean.TRUE, till.estaEbria());
        assertEquals(Boolean.TRUE, richard.estaEbria());

        // PUNTO 4:
        assertEquals(Boolean.FALSE, berdych.quiereEntrar(carpa5));
        assertEquals(Boolean.TRUE, dirk.quiereEntrar(carpa2));

        // PUNTO 5:
        assertEquals(Boolean.FALSE, carpa1.dejarEntrar(richard));
        assertEquals(Boolean.TRUE, carpa1.dejarEntrar(navratilova));

        // PUNTO 6:
        assertEquals(Boolean.TRUE, vanDamme.puedeEntrar(carpa3));
        assertEquals(Boolean.TRUE, vanDamme.puedeEntrar(carpa6));
        assertEquals(Boolean.FALSE, vanDamme.puedeEntrar(carpa1));

        // PUNTO 7:
        carpa5.ingresar(vanDamme);

        assertEquals(Arrays.asList(vanDamme), carpa5.getGenteAdentro());

        // PUNTO 8:
        carpa2.ingresar(dirk);
        dirk.comprarJarra(jarra4);
        dirk.comprarJarra(jarra4);
        dirk.comprarJarra(jarra4);

        assertEquals(Boolean.TRUE, dirk.estaEbria());
        assertEquals((Integer) 1, carpa2.cantidadDeEbriosEmpedernidos());

        // PUNTO 9:
        carpa3.ingresar(vanDamme);
        vanDamme.comprarJarra(jarra5);
        vanDamme.comprarJarra(jarra6);

        assertEquals(Boolean.FALSE, dirk.esPatriota());
        assertEquals(Boolean.TRUE, richard.esPatriota());
        assertEquals(Boolean.TRUE, vanDamme.esPatriota());

        // PUNTO 10:
        assertEquals(Arrays.asList(carpa2), dirk.carpasEnLasQueCompro());
        assertEquals(Arrays.asList(carpa3), vanDamme.carpasEnLasQueCompro());

        // PUNTO 11:
        carpa6.ingresar(christoph);
        carpa6.ingresar(oliver);
        christoph.comprarJarra(jarra11);
        oliver.comprarJarra(jarra12);
        carpa4.ingresar(christoph);
        carpa4.ingresar(oliver);
        christoph.comprarJarra(jarra7);
        oliver.comprarJarra(jarra8);

        assertEquals(Boolean.TRUE, till.fueJuntoA(dirk));
        assertEquals(Boolean.FALSE, vanDamme.fueJuntoA(till));
        assertEquals(Boolean.TRUE, christoph.fueJuntoA(oliver));
    }
}

