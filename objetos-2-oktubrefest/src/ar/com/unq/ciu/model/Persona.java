package ar.com.unq.ciu.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public abstract class Persona {

    protected Integer peso;
    protected Boolean leGustaLaMusicaTradicional;
    protected Integer nivelDeAguante;
    protected Collection<Jarra> jarrasCompradas;

    public Persona(Integer peso, Boolean leGustaLaMusicaTradicional, Integer nivelDeAguante) {
        this.peso = peso;
        this.leGustaLaMusicaTradicional = leGustaLaMusicaTradicional;
        this.nivelDeAguante = nivelDeAguante;
        this.jarrasCompradas = new ArrayList<Jarra>();
    }

    protected abstract Boolean leGusta(MarcaDeCerveza marca);

    // PUNTO 2:
    public Double alcoholIngerido() {
        return this.jarrasCompradas.stream()
                .mapToDouble(jarra -> jarra.cantidadDeAlcohol())
                .sum();
    }

    // PUNTO 3:
    public Boolean estaEbria() {
        return (this.alcoholIngerido() * this.peso) > this.nivelDeAguante;
    }

    // PUNTO 4:
    public Boolean quiereEntrar(Carpa carpa) {
        return this.leGusta(carpa.getMarca()) && this.leGustaLaMusicaTradicional.equals(carpa.getTieneBanda());
    }

    // PUNTO 6:
    public Boolean puedeEntrar(Carpa carpa) {
        return this.quiereEntrar(carpa) && carpa.dejarEntrar(this);
    }

    // PUNTO 9:
    public abstract Boolean esPatriota();

    // PUNTO 10:
    public Collection<Carpa> carpasEnLasQueCompro() {
        return this.jarrasCompradas.stream()
                .map(jarra -> jarra.getProvenienteDeCarpa())
                .distinct()
                .collect(Collectors.toList());
    }

    // PUNTO 11:
    public Boolean fueJuntoA(Persona persona) {
        return this.carpasEnLasQueCompro().stream()
                .allMatch(carpa -> persona.carpasEnLasQueCompro().contains(carpa));
    }

    public void comprarJarra(Jarra jarra) {
        jarrasCompradas.add(jarra);
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public Boolean getLeGustaLaMusicaTradicional() {
        return leGustaLaMusicaTradicional;
    }

    public void setLeGustaLaMusicaTradicional(Boolean leGustaLaMusicaTradicional) {
        this.leGustaLaMusicaTradicional = leGustaLaMusicaTradicional;
    }

    public Integer getNivelDeAguante() {
        return nivelDeAguante;
    }

    public void setNivelDeAguante(Integer nivelDeAguante) {
        this.nivelDeAguante = nivelDeAguante;
    }

    public Collection<Jarra> getJarrasCompradas() {
        return jarrasCompradas;
    }

    public void setJarrasCompradas(Collection<Jarra> jarrasCompradas) {
        this.jarrasCompradas = jarrasCompradas;
    }
}
