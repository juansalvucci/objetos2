package ar.com.unq.ciu.model;

public final class MarcaDeCervezaRoja extends MarcaDeCervezaNegra {

    public MarcaDeCervezaRoja(Double contenidoDeLupulo, String paisDeOrigen) {
        super(contenidoDeLupulo, paisDeOrigen);
    }

    @Override
    protected Double graduacionAlcoholica() {
        return super.graduacionAlcoholica() * 1.25;
    }
}
