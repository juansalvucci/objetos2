package ar.com.unq.ciu.model;

public class MarcaDeCervezaNegra extends MarcaDeCerveza {

    private static Double graduacionReglamentaria = 5.0;

    public MarcaDeCervezaNegra(Double contenidoDeLupulo, String paisDeOrigen) {
        super(contenidoDeLupulo, paisDeOrigen);
    }

    @Override
    protected Double graduacionAlcoholica() {
        return Math.min(graduacionReglamentaria, this.getContenidoDeLupulo() * 2);
    }

    public static Double getGraduacionReglamentaria() {
        return graduacionReglamentaria;
    }

    public static void setGraduacionReglamentaria(Double graduacionReglamentaria) {
        MarcaDeCervezaNegra.graduacionReglamentaria = graduacionReglamentaria;
    }
}
