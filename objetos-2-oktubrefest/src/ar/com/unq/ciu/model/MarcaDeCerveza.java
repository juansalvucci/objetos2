package ar.com.unq.ciu.model;

public abstract class MarcaDeCerveza {

    protected Double contenidoDeLupulo;
    protected String paisDeOrigen;

    public MarcaDeCerveza(Double contenidoDeLupulo, String paisDeOrigen) {
        this.contenidoDeLupulo = contenidoDeLupulo;
        this.paisDeOrigen = paisDeOrigen;
    }

    protected abstract Double graduacionAlcoholica(); // (Porcentaje)

    public Double getContenidoDeLupulo() {
        return contenidoDeLupulo;
    }

    public void setContenidoDeLupulo(Double contenidoDeLupulo) {
        this.contenidoDeLupulo = contenidoDeLupulo;
    }

    public String getPaisDeOrigen() {
        return paisDeOrigen;
    }

    public void setPaisDeOrigen(String paisDeOrigen) {
        this.paisDeOrigen = paisDeOrigen;
    }
}
