package ar.com.unq.ciu.model;

public final class PersonaAlemana extends Persona {

    private static final String paisDeOrigen = "Alemania";

    public PersonaAlemana(Integer peso, Boolean leGustaLaMusicaTradicional, Integer nivelDeAguante) {
        super(peso, leGustaLaMusicaTradicional, nivelDeAguante);
    }

    @Override
    protected Boolean leGusta(MarcaDeCerveza marca) {
        return true;
    }

    @Override
    public Boolean quiereEntrar(Carpa unaCarpa) {
        return super.quiereEntrar(unaCarpa) && (unaCarpa.getGenteAdentro().size() % 2 == 0);
    }

    @Override
    public Boolean esPatriota() {
        return this.jarrasCompradas.stream()
                .allMatch(jarra -> jarra.getProvenienteDeCarpa().getMarca().paisDeOrigen.equals(this.paisDeOrigen));
    }

    public static String getPaisDeOrigen() {
        return paisDeOrigen;
    }
}
