package ar.com.unq.ciu.model;

public final class Jarra {

    private Double capacidad;  // litros
    private Carpa provenienteDeCarpa;

    public Jarra(Double capacidad, Carpa provenienteDeCarpa) {
        this.capacidad = capacidad;
        this.provenienteDeCarpa = provenienteDeCarpa;
    }

    // PUNTO 1:
    public Double cantidadDeAlcohol() {
        return this.capacidad * (this.provenienteDeCarpa.getMarca().graduacionAlcoholica() / 100);
    }

    public Double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Double capacidad) {
        this.capacidad = capacidad;
    }

    public Carpa getProvenienteDeCarpa() {
        return provenienteDeCarpa;
    }

    public void setProvenienteDeCarpa(Carpa provenienteDe) {
        this.provenienteDeCarpa = provenienteDe;
    }
}
