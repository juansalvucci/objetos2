package ar.com.unq.ciu.model;

public final class PersonaCheca extends Persona {

    private static final String paisDeOrigen = "Republica Checa";

    public PersonaCheca(Integer peso, Boolean leGustaLaMusicaTradicional, Integer nivelDeAguante) {
        super(peso, leGustaLaMusicaTradicional, nivelDeAguante);
    }

    @Override
    protected Boolean leGusta(MarcaDeCerveza marca) {
        return marca.graduacionAlcoholica() > 8;
    }

    @Override
    public Boolean esPatriota() {
        return this.jarrasCompradas.stream()
                .allMatch(jarra -> jarra.getProvenienteDeCarpa().getMarca().paisDeOrigen.equals(this.paisDeOrigen));
    }

    public static String getPaisDeOrigen() {
        return paisDeOrigen;
    }
}

