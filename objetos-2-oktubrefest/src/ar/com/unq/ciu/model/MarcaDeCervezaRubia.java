package ar.com.unq.ciu.model;

public final class MarcaDeCervezaRubia extends MarcaDeCerveza {

    protected Double graduacionAlcoholica;

    public MarcaDeCervezaRubia(Double contenidoDeLupulo, String paisDeOrigen, Double graduacionAlcoholica) {
        super(contenidoDeLupulo, paisDeOrigen);
        this.graduacionAlcoholica = graduacionAlcoholica;
    }

    @Override
    public Double graduacionAlcoholica() {
        return this.getGraduacionAlcoholica();
    }

    public Double getGraduacionAlcoholica() {
        return graduacionAlcoholica;
    }

    public void setGraduacionAlcoholica(Double graduacionAlcoholica) {
        this.graduacionAlcoholica = graduacionAlcoholica;
    }
}
