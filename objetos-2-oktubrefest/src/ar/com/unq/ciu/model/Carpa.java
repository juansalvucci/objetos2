package ar.com.unq.ciu.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public final class Carpa {

    private Integer capacidadDeGente;
    private Boolean tieneBanda;
    private MarcaDeCerveza marca;
    private Collection<Persona> genteAdentro;

    public Carpa(Integer capacidadDeGente, Boolean tieneBanda, MarcaDeCerveza marca) {
        this.capacidadDeGente = capacidadDeGente;
        this.tieneBanda = tieneBanda;
        this.marca = marca;
        this.genteAdentro = new ArrayList<Persona>();
    }

    // PUNTO 5:
    public Boolean dejarEntrar(Persona persona) {
        return (this.genteAdentro.size() < this.capacidadDeGente) && !persona.estaEbria();
    }

    // PUNTO 7:
    public void ingresar(Persona persona) {
        if (!persona.puedeEntrar(this)) {
            System.out.println("No puede ingresar");
        } else {
            genteAdentro.add(persona);
        }
    }

    // PUNTO 8:
    private Collection<Persona> ebrios() {
        return this.genteAdentro.stream()
                .filter(persona -> persona.estaEbria())
                .collect(Collectors.toList());
    }

    public Integer cantidadDeEbriosEmpedernidos() {
        return this.ebrios().stream()
                .filter(ebrio -> ebrio.getJarrasCompradas().stream()
                        .allMatch(jarra -> jarra.getCapacidad() >= 1.0))
                .collect(Collectors.toList())
                .size();
    }

    public Integer getCapacidadDeGente() {
        return capacidadDeGente;
    }

    public void setCapacidadDeGente(Integer capacidadDeGente) {
        this.capacidadDeGente = capacidadDeGente;
    }

    public Boolean getTieneBanda() {
        return tieneBanda;
    }

    public void setTieneBanda(Boolean tieneBanda) {
        this.tieneBanda = tieneBanda;
    }

    public MarcaDeCerveza getMarca() {
        return marca;
    }

    public void setMarca(MarcaDeCerveza marca) {
        this.marca = marca;
    }

    public Collection<Persona> getGenteAdentro() {
        return genteAdentro;
    }

    public void setGenteAdentro(Collection<Persona> genteAdentro) {
        this.genteAdentro = genteAdentro;
    }
}
