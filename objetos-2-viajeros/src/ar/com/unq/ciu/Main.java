package ar.com.unq.ciu;

import ar.com.unq.ciu.modelo.*;

import java.util.Collection;

public class Main {

    public static void main(String[] args) {

        // Paises:
        Pais paris = new Pais("Paris");
        Pais madrid = new Pais("Madrid");
        Pais londres = new Pais("Londres");
        Pais berlin = new Pais("Berlin");

        // Viajes:
        Viaje viaje1 = new Viaje(paris, 2014);
        Viaje viaje2 = new Viaje(madrid, 2019);
        Viaje viaje3 = new Viaje(londres, 2019);
        Viaje viaje4 = new Viaje(berlin, 2014);
        Viaje viaje5 = new Viaje(londres, 2019);

        // Viajeros:
        Establecido juan = new Establecido("Juan", "Perez", berlin);
        Doctor pedro = new Doctor("Pedro", "Rodriguez", londres, berlin, 2015, 2019);
        Migrante antonia = new Migrante("Antonia", "Garcia", paris, londres, 2019);
        Menor carlos = new Menor("Carlos", "Garcia", antonia);

        pedro.agregarViaje(viaje4);
        pedro.agregarViaje(viaje5);
        antonia.agregarViaje(viaje3);
        carlos.agregarViaje(viaje3);


        System.out.println("Paises en los que estuvo Pedro en el anio 2019: " + pedro.enQuePaisesEstuvo(2019));

        System.out.println("Paises en los que estuvo Antonia en el anio 2019: " + antonia.enQuePaisesEstuvo(2019));

        System.out.println("Coincidieron Antonia y Pedro en algun pais en el anio 2019: " + antonia.coincidioCon(pedro, 2019));

        System.out.println("Paises en los que estuvo Carlos, el hijo de Antonia, en el anio 2019: " + carlos.enQuePaisesEstuvo(2019));

        System.out.println("Paises en los que estuvo de Juan en el anio 2020: " + juan.enQuePaisesEstuvo(2020));

        System.out.println("Coincidieron Antonia y Juan en algun pais en el anio 2019: " + antonia.coincidioCon(juan, 2019));

    }
}
