package ar.com.unq.ciu.modelo;

import java.util.ArrayList;
import java.util.Collection;

public class Migrante extends Persona {
    private Pais nacioEn;
    private Pais seMudoA;
    private Integer anioEnQueSeMudo;

    public Migrante(String nombre, String apellido, Pais nacioEn, Pais seMudoA, Integer anioEnQueSeMudo) {
        super(nombre, apellido);
        this.nacioEn = nacioEn;
        this.seMudoA = seMudoA;
        this.anioEnQueSeMudo = anioEnQueSeMudo;
    }

    @Override
    public Collection<Pais> paisesDeResidencia(Integer unAnio) {
        Collection<Pais> paises = new ArrayList<>();

        if (unAnio < this.anioEnQueSeMudo) {
            paises.add(this.nacioEn);
        } else if (unAnio == this.anioEnQueSeMudo) {
            paises.add(this.nacioEn);
            paises.add(this.seMudoA);
        } else {
            paises.add(this.seMudoA);
        }
        return paises;
    }

    public Pais getNacioEn() {
        return nacioEn;
    }

    public Pais getSeMudoA() {
        return seMudoA;
    }

    public Integer getAnioEnQueSeMudo() {
        return anioEnQueSeMudo;
    }

    public void setNacioEn(Pais nacioEn) {
        this.nacioEn = nacioEn;
    }

    public void setSeMudoA(Pais seMudoA) {
        this.seMudoA = seMudoA;
    }

    public void setAnioEnQueSeMudo(Integer anioEnQueSeMudo) {
        this.anioEnQueSeMudo = anioEnQueSeMudo;
    }

}
