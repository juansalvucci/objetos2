package ar.com.unq.ciu.modelo;

public class Viaje {

    public Pais pais;
    public Integer anio;

    public Viaje(Pais pais, Integer anio) {
        this.pais = pais;
        this.anio = anio;
    }

    public Pais getPais() {
        return pais;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Override
    public String toString() {
        return "Viaje{" +
                "pais='" + pais + '\'' +
                ", anio=" + anio +
                '}';
    }
}
