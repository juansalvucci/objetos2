package ar.com.unq.ciu.modelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public abstract class Persona {

    private String nombre;
    private String apellido;
    private Collection<Viaje> viajesRealizados;

    public Persona(String nombre, String apellido) {
        super();
        this.nombre = nombre;
        this.apellido = apellido;
        this.viajesRealizados = new ArrayList<>();
    }

    public abstract Collection<Pais> paisesDeResidencia(Integer unAnio);

    public void agregarViaje(Viaje unViaje) {
        this.viajesRealizados.add(unViaje);
    }

    public Collection<Pais> enQuePaisesEstuvo(Integer unAnio) {
        Collection<Pais> paisesResidencia = this.paisesDeResidencia(unAnio);

        Collection<Pais> paises = this.viajesRealizados.stream()
                .filter(viaje -> viaje.getAnio().equals(unAnio))
                .map(viaje -> viaje.getPais())
                .collect(Collectors.toList());

        paises.addAll(paisesResidencia);

        return paises;
    }

    public boolean coincidioCon(Persona persona, Integer unAnio) {
        Collection<Pais> paises1 = this.enQuePaisesEstuvo(unAnio);
        Collection<Pais> paises2 = persona.enQuePaisesEstuvo(unAnio);

        return paises1.stream()
                .filter(pais -> paises2.contains(pais))
                .collect(Collectors.toList())
                .size() > 0;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                '}';
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setViajesRealizados(Collection<Viaje> viajesRealizados) {
        this.viajesRealizados = viajesRealizados;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public Collection<Viaje> getViajesRealizados() {
        return viajesRealizados;
    }

}
