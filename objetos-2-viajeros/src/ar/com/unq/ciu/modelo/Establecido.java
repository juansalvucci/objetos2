package ar.com.unq.ciu.modelo;

import java.util.ArrayList;
import java.util.Collection;

public class Establecido extends Persona {

    private Pais paisDeResidencia;

    public Establecido(String nombre, String apellido, Pais paisDeResidencia) {
        super(nombre, apellido);
        this.paisDeResidencia = paisDeResidencia;
    }

    @Override
    public Collection<Pais> paisesDeResidencia(Integer unAnio) {
        Collection<Pais> paises = new ArrayList<>();
        paises.add(paisDeResidencia);

        return paises;
    }

    public Pais getPaisDeResidencia() {
        return paisDeResidencia;
    }

    public void setPaisDeResidencia(Pais paisDeResidencia) {
        this.paisDeResidencia = paisDeResidencia;
    }
}
