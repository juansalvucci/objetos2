package ar.com.unq.ciu.modelo;

import java.util.ArrayList;
import java.util.Collection;

public class Doctor extends Persona {

    private Pais paisDeResidencia;
    private Pais paisDoctorado;
    private Integer anioInicioDoctorado;
    private Integer anioFinDoctorado;

    public Doctor(String nombre, String apellido, Pais paisDeResidencia, Pais paisDoctorado, Integer anioInicioDoctorado, Integer anioFinDoctorado) {
        super(nombre, apellido);
        this.paisDeResidencia = paisDeResidencia;
        this.paisDoctorado = paisDoctorado;
        this.anioInicioDoctorado = anioInicioDoctorado;
        this.anioFinDoctorado = anioFinDoctorado;
    }

    @Override
    public Collection<Pais> paisesDeResidencia(Integer unAnio) {
        Collection<Pais> paises = new ArrayList<>();

        if (unAnio == this.anioInicioDoctorado || unAnio == this.anioFinDoctorado) {
            paises.add(this.paisDeResidencia);
            paises.add(this.paisDoctorado);
        } else if (unAnio > this.anioFinDoctorado || unAnio < this.anioInicioDoctorado) {
            paises.add(this.paisDeResidencia);
        } else {
            paises.add(this.paisDoctorado);
        }
        return paises;
    }

    public Pais getPaisDeResidencia() {
        return paisDeResidencia;
    }

    public Pais getPaisDoctorado() {
        return paisDoctorado;
    }

    public Integer getAnioInicioDoctorado() {
        return anioInicioDoctorado;
    }

    public Integer getAnioFinDoctorado() {
        return anioFinDoctorado;
    }

    public void setPaisDeResidencia(Pais paisDeResidencia) {
        this.paisDeResidencia = paisDeResidencia;
    }

    public void setPaisDoctorado(Pais paisDoctorado) {
        this.paisDoctorado = paisDoctorado;
    }

    public void setAnioInicioDoctorado(Integer anioInicioDoctorado) {
        this.anioInicioDoctorado = anioInicioDoctorado;
    }

    public void setAnioFinDoctorado(Integer anioFinDoctorado) {
        this.anioFinDoctorado = anioFinDoctorado;
    }
}
