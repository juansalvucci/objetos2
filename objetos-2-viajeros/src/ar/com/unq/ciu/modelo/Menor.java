package ar.com.unq.ciu.modelo;

import java.util.ArrayList;
import java.util.Collection;

public class Menor extends Persona {
    private Persona madre;


    public Menor(String nombre, String apellido, Persona madre) {
        super(nombre, apellido);
        this.madre = madre;
    }

    @Override
    public Collection<Pais> paisesDeResidencia(Integer unAnio) {
        return madre.paisesDeResidencia(unAnio);
    }

    public Persona getMadre() {
        return madre;
    }

    public void setMadre(Persona madre) {
        this.madre = madre;
    }

}
